# Updater SquareSpace Template

---

## Why a Separate Repo

SquareSpace itself doesn't offer a good collaboration workflow as it lacks such critical features as pull/merge requests, source code viewers, git history, etc. While it's a convenient and efficient alternative to traditional FTPs, SquareSpace Developer Mode is optimized for a one person operation and doesn't account for contributions from multiple people.

- Do not `git push` any commits directly to the master repo on the SquareSpace server
- Create a relevant feature branch, push to `gitlab` remote repo, and create a merge request.


## Recommended Workflow for Team Members

1. Clone `gitlab` repo to your local computer
```bash
$ git clone https://gitlab.com/baadaa/upd-squarespace.git
$ cd upd-squarespace
```
2. Make sure the `origin` is pointed at Gitlab repo. 
```bash
$ git remote -v
origin	https://gitlab.com/baadaa/upd-squarespace.git (fetch)
origin	https://gitlab.com/baadaa/upd-squarespace.git (push)
```
3. Create a feature branch and edit the code
```bash
$ git branch name-of-branch
$ git checkout name-of-branch
```
4. Once done, push to `origin` and create a merge request


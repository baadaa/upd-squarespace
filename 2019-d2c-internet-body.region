<main>
  <section class="d2c-internet-2019">
    <section class="d2c-internet-2019--hero">
      <section class="d2c-internet-2019--hero-heading container-2018">
        <span class="d2c-internet-2019--providers-list">
          <h5>Top providers include:</h5>
          <ul>
            <li>Armstrong</li>
            <li>AT&T</li>
            <li>Atlantic Broadband</li>
            <li>BendBroadband</li>
            <li>Buckeye Broadband</li>
            <li>Cable ONE</li>
            <li>CenturyLink</li>
            <li>Cincinnati Bell</li>
            <li>Cox</li>
            <li>DIRECTV</li>
            <li>DISH</li>
            <li>EarthLink</li>
            <li>Frontier</li>
            <li>GCI</li>
            <li>Grande Communications</li>
            <li>HughesNet</li>
            <li>Mediacom</li>
            <li>MegaPath</li>
            <li>Midco</li>
            <li>One</li>
            <li>OneSource</li>
            <li>Optimum</li>
            <li>Pixius</li>
            <li>Primus (Canadian company)</li>
            <li>RCN</li>
            <li>Rise Broadband</li>
            <li>Spectrum</li>
            <li>TDS</li>
            <li>Verizon</li>
            <li>Viaero</li>
            <li>Viasat</li>
            <li>Vodafone</li>
            <li>Windstream</li>
            <li>WOW!</li>
            <li>Xfinity</li>
            <li class="close-provider-list">
              <img
                src="https://res.cloudinary.com/updater-marketing/image/upload/v1557549472/website/2019-d2c-direction/ui-elements/close-x.svg"
                alt="" />
            </li>
          </ul>
        </span>
        <h1>Check Internet and TV<br>Off Your To-Do List.</h1>
        <h2>Free to call, free to schedule &mdash; just plain free</h2>
        <ul>
          <li>We find you the best price, guaranteed
          </li>
          <li>Installation is scheduled for you</li>
          <li>Access to <span class="d2c-internet-2019--providers">25+ providers</span></li>
          <li>No wait time</li>
        </ul>
        <a id="kandela-lp_call-us-top" href="tel:833-200-1226">Call us at <span>(833) 200-1226</span></a>
        <div id="kandela-lp_how-it-works" class="scroll-prompt">
          How it works<br>
          <img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1555083744/website/2019-d2c-direction/icons/scroll-prompt-navy.svg"
            alt="scroll down">
        </div>
      </section>
    </section>
    <section class="d2c-internet-2019--how-it-works container-2018" id="how-it-works">
      <h2>We do the work, so you don't have to.</h2>
      <section class="d2c-internet-2019--grid">
        <section class="d2c-internet-2019--grid-item step1">
          <div>
            <h3>1</h3>
            <h4>Give us a call <em>anytime</em></h4>
            <p>No wait time, no elevator&nbsp;music.</p>
          </div>
          <svg>
            <use xlink:href="/assets/d2c-internet-svg-illo-defs.svg#d2c-internet--call"></use>
          </svg>
        </section>
        <section class="d2c-internet-2019--grid-item step2">
          <div>
            <h3>2</h3>
            <h4>Find the best deal</h4>
            <p>We find you the best package that meets <em>your</em> needs.</p>
          </div>
          <svg>
            <use xlink:href="/assets/d2c-internet-svg-illo-defs.svg#d2c-internet--find"></use>
          </svg>
        </section>
        <section class="d2c-internet-2019--grid-item step3">
          <div>
            <h3>3</h3>
            <h4>Schedule an installation</h4>
            <p>We schedule an installation time that fits <em>your</em> schedule.</p>
          </div>
          <svg>
            <use xlink:href="/assets/d2c-internet-svg-illo-defs.svg#d2c-internet--schedule"></use>
          </svg>
        </section>
        <section class="d2c-internet-2019--grid-item step4">
          <div>
            <h3>4</h3>
            <h4><span>(Optional)</span> Throw&nbsp;a&nbsp;party</h4>
            <p>...with the money you saved on internet and TV.</p>
          </div>
          <svg>
            <use xlink:href="/assets/d2c-internet-svg-illo-defs.svg#d2c-internet--confetti"></use>
          </svg>
        </section>
      </section>
      <h5>Ready to check internet and TV off your list?<br>
        We're here to chat 24/7</h5>
      <a id="kandela-lp_call-us-bottom" href="tel:8332001226">Call (833) 200-1226</a>
    </section>
    <section class="d2c-2019--home-testimonial">
      <div class="d2c-2019--home-testimonial-carousel container-2018">
        <h3>See what people are saying</h3>
        <div class="d2c-2019--home-carousel-container" data-flickity='{ "wrapAround": true, "autoPlay": 5000 }'>
          <div class="d2c-2019--home-carousel-inner">
            <div class="d2c-2019--home-carousel-quote">
              I didn't realize how much I was overspending on my internet until I
              chatted with someone from Updater. They saved me a ton of money and I was even able to get faster
              internet!
              <span class="quote-by">
                &mdash; Ruth P.
              </span>
              <span class="moving-to">
                New York, NY
              </span>
            </div>
            <div class="d2c-2019--home-carousel-photo">
              <img
                src="https://res.cloudinary.com/updater-marketing/image/upload/v1561411163/website/2019-d2c-direction/big-testimonial-shots/ruth.jpg"
                alt="Ruth">
            </div>
          </div>
          <div class="d2c-2019--home-carousel-inner">
            <div class="d2c-2019--home-carousel-quote">
              Talking to the cable company is never fun so I was procrastinating calling
              to set up my internet and TV. I gave Updater a call instead. For once, scheduling an appointment was
              painless. Thanks, guys!
              <span class="quote-by">
                &mdash; Dale A.
              </span>
              <span class="moving-to">
                Minneapolis, MN
              </span>
            </div>
            <div class="d2c-2019--home-carousel-photo">
              <img
                src="https://res.cloudinary.com/updater-marketing/image/upload/v1561412504/website/2019-d2c-direction/big-testimonial-shots/dale_578580478_3.jpg"
                alt="Dale">
            </div>
          </div>
          <div class="d2c-2019--home-carousel-inner">
            <div class="d2c-2019--home-carousel-quote">
              I wanted to bundle cable to my already-existing internet but wasn't sold
              on what my provider was offering. When I called Updater they walked me through all of my options and
              weren't pushy. I hate pushy.
              <span class="quote-by">
                &mdash; Christine K.
              </span>
              <span class="moving-to">
                Phoenix, AZ
              </span>
            </div>
            <div class="d2c-2019--home-carousel-photo">
              <img
                src="https://res.cloudinary.com/updater-marketing/image/upload/v1561411826/website/2019-d2c-direction/big-testimonial-shots/christine_530445778.jpg"
                alt="Christine">
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="d2c-internet-2019--moving-tips blog-preview-items">
      <div class="container-2018" style="text-align: center;">
        <h2>Moving Tips, Tricks, and Advice</h2>
        <div class="blog-preview-2018" data-flickity='{ "wrapAround": true }'>
          <squarespace:query collection="moving-tips" limit="6">
            {.repeated section items}
            <div class="blog-preview-item-2018">
              <a href="{fullUrl}" class="thumb">
                <img class="wo_cloudinary__blog-preview" src="{assetUrl}?format=500w" alt="{title}">
              </a>
              <span class="blog-list--item-date">
                <time datetime="{addedOn|date %F}">{addedOn|date %B %d, %Y}</time>
              </span>
              <h4 class="title">
                <a href="{fullUrl}">{title}</a>
              </h4>
            </div>
            {.end}
          </squarespace:query>
        </div>
        <a class="blue-btn-outline-2018" href="/moving-tips/">Browse more moving tips</a>
      </div>
    </section>

  </section>
</main>

<squarespace:script src="flickity.pkgd.min.js" combo="true" />
<squarespace:script src="upd-ui-bits_20_07.min.js" combo="true" />

<script>
  function init() {
    function handleJump(e) {
      e.preventDefault();
      var targetElem = document.querySelector('#how-it-works');
      window.scrollTo({
        top: targetElem.offsetTop - 120,
        behavior: 'smooth'
      })
    }

    function toggleProviderList() {
      var providerList = document.querySelector('.d2c-internet-2019--providers-list');
      providerList.classList.toggle('is-on');
    }

    var providerPrompt = document.querySelector('.d2c-internet-2019--providers');
    var closeProvider = document.querySelector('.close-provider-list');

    initNav();
    [providerPrompt, closeProvider].forEach(function (item) {
      item.addEventListener('click', toggleProviderList);
    })
    document.querySelector('.scroll-prompt').addEventListener('click', handleJump);
  }
  window.addEventListener('DOMContentLoaded', init);
</script>
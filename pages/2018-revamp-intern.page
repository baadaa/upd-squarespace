<section class="jobs-2018">
  <div class="breadcrumb-2018">
    <nav class="container-2018">
      <ul>
        <li><a href="/about-us/">About</a></li>
        <li><a href="/jobs/">Jobs</a></li>
        <li><a href="/jobs/internships/">Internships</a></li>
      </ul>
    </nav>
  </div>
  <section class="jobs-2018--intro container-2018">
    <section class="jobs-2018--intro-video">
      <a href="https://www.youtube.com/embed/_vc4MeUnwuE?rel=0&amp;autoplay=1" data-featherlight="iframe"
        data-featherlight-iframe-frameborder="0" data-featherlight-iframe-allow="autoplay; encrypted-media"
        data-featherlight-iframe-allowfullscreen="true"
        data-featherlight-iframe-style="display:block;border:none;height:85vh;width:85vw;">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/f_auto,q_auto/v1545616257/website/2018-revamp-assets/thumbnails/video-thumb_internship_2.jpg"
          alt="This is Updater Video" class="jobs-video-thumb">
      </a>
    </section>
    <section class="jobs-2018--intro-copy">
      <squarespace:block-field id="jobs-2018--internship-block-field" />
    </section>
  </section>
  <section class="jobs-2018--recognition">
    <h6>Some of the companies that hired Updater interns</h6>
    <section class="jobs-2018--recognition-logos intern container-2018">
      <img
        src="https://res.cloudinary.com/updater-marketing/image/upload/c_fit,f_auto,q_auto,w_80,h_50/v1502214199/partners/logos/cnn.png"
        alt="CNN">
      <img
        src="https://res.cloudinary.com/updater-marketing/image/upload/v1526999007/partners/logos/meetup-logo-script.svg"
        alt="Meetup">
      <img src="https://res.cloudinary.com/updater-marketing/image/upload/v1502214198/partners/logos/ventureamerica.png"
        alt="Venture for America">
      <img
        src="https://res.cloudinary.com/updater-marketing/image/upload/c_fit,f_auto,q_auto,w_100,h_50/v1526998942/partners/logos/QuickenLoans-Logo-Stack.png"
        alt="Quicken Loans">
      <img
        src="https://res.cloudinary.com/updater-marketing/image/upload/v1502214198/partners/logos/GrubHub_Logo_2016.svg"
        alt="Grubhub">
      <img
        src="https://res.cloudinary.com/updater-marketing/image/upload/v1559151374/partners/logos/Fox_News_Channel_logo.svg"
        alt="Fox News">
      <img src="https://res.cloudinary.com/updater-marketing/image/upload/logos/updater_logo.svg" alt="Updater">
    </section>
  </section>
  <section class="internship-2018--two-cards">
    <h2>Seeking Bright Talents Each Semester</h2>
    <section class="internship-2018--two-cards-wrapper container-2018">
      <section class="intern-card">
        <squarespace:block-field id="jobs-2018--internship-what-you-bring-block-field" />
      </section>
      <section class="intern-card">
        <squarespace:block-field id="jobs-2018--internship-what-we-provide-block-field" />
      </section>
    </section>
    <a class="card-cta" href="/jobs/openings/">View Openings</a>
  </section>
  <section class="jobs-2018--recognition">
    <h6>Recognized for awesome work culture</h6>
    <section class="jobs-2018--recognition-logos intern container-2018">
      <div class="big-logo-container">
        <a href="/updater-life/updater-named-crains-best-places-to-work-2018">
          <img
            src="https://res.cloudinary.com/updater-marketing/image/upload/c_pad,h_250,w_400,q_auto,f_auto/v1507081005/partners/logos/cnyb_logo_black.png"
            class="big-logo" alt="Crain's">
          Top 100 Places<br>to Work in NYC
        </a>
      </div>
      <div class="big-logo-container">
        <a href="/updater-life/updater-lands-top-internship-rank">
          <img
            src="https://res.cloudinary.com/updater-marketing/image/upload/c_pad,h_200,w_400,q_auto,f_auto/v1545839035/website/2018-revamp-assets/logos/wayup_primary_horizontal_rgb_2018.png"
            class="big-logo" alt="WayUp">
          #54 Top 100<br>Internship Programs
        </a>
      </div>
      <div class="big-logo-container">
        <a href="/updater-life/september-at-updater-acquisitions-awards-and-offsite">
          <img
            src="https://res.cloudinary.com/updater-marketing/image/upload/c_pad,h_250,w_400,q_auto,f_auto/v1545619216/website/2018-revamp-assets/logos/Timmy-Awards-Logo-e1504720897590-850x396.png"
            class="big-logo" alt="Timmy Awards">
          Best Tech<br>Work Culture
        </a>
      </div>
      <div class="big-logo-container">
        <a href="/updater-life/best-place-to-work-2019-builtin">
          <img
            src="https://res.cloudinary.com/updater-marketing/image/upload/c_pad,h_250,w_400,q_auto,f_auto/v1545619216/website/2018-revamp-assets/logos/BestPlacesToWork2019_MidsizeCo_Vertical_NYC.svg"
            class="big-logo" style="max-height: 160px!important;" alt="Built In NYC Best Places to Work">
          Best Places<br>to Work
        </a>
      </div>
    </section>
  </section>
  <section class="internship-2018--testimonials container-2018">
    <h2>Hear from our former interns</h2>
    <section class="internship-2018--testimonials-wrapper">
      <section class="internship-2018--testimonials-card">
        <div class="intern-info">
          <img
            src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,w_400/v1501000670/team%20photos/Headshots/Madison_Escobar.jpg"
            alt="Madison Escobar" class="intern-headshot">
          <div class="intern-txt-block">
            <span class="intern-name">Madison Escobar</span>
            <span class="intern-title">Former Marketing Intern, Currently Recruiting Coordinator at Updater</span>
          </div>
        </div>
        <p class="intern-testimonial">The Updater team has been instrumental in shaping my career. Having the
          opportunity to work with two different teams as an intern was an invaluable experience. I can't thank them
          enough for this opportunity!
        </p>
      </section>
      <section class="internship-2018--testimonials-card">
        <div class="intern-info">
          <img
            src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,w_400/v1446834125/team%20photos/Headshots/Jenna-Davis.jpg"
            alt="Jenna Davis" class="intern-headshot">
          <div class="intern-txt-block">
            <span class="intern-name">Jenna Davis</span>
            <span class="intern-title">Former Marketing and Business Operations Intern</span>
          </div>
        </div>
        <p class="intern-testimonial">As an intern at Updater, you'll receive incredible mentorship and support from the
          team. You'll also be pushed to explore topics outside of your everyday skill set that will ultimately help you
          develop your career path.
        </p>
      </section>
      <section class="internship-2018--testimonials-card">
        <div class="intern-info">
          <img
            src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,w_400/v1497376599/team%20photos/Headshots/team_headshots_jamesw.jpg"
            alt="James Wong" class="intern-headshot">
          <div class="intern-txt-block">
            <span class="intern-name">James Wong</span>
            <span class="intern-title">Former Business Development Intern</span>
          </div>
        </div>
        <p class="intern-testimonial">One of the best parts of the internship program at Updater is that you're helping
          to support multiple teams at once. I walked away from my internship with a well-rounded set of professional
          skills.
        </p>
      </section>
    </section>

  </section>
  <section class="internship-2018--photo-feed">
    <h2>Intern Life at Updater</h2>
    <section class="jobs-2018--photo-feed-grid container-2018">
      <div id="behindthescenes2" class="flex-images">
        <div class="item" data-w="5184" data-h="3456"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549871/jobs%20photos/internships/Summer2017.jpg">
        </div>

        <div class="item" data-w="720" data-h="1280"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1513970124/jobs%20photos/internships/charlie_chocolate_factory.jpg">
        </div>

        <div class="item" data-w="852" data-h="640"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549874/jobs%20photos/internships/RonBurgandy.jpg">
        </div>

        <div class="item" data-w="1632" data-h="872"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549871/jobs%20photos/internships/Amanda-Jenna-2.jpg">
        </div>

        <div class="item" data-w="1188" data-h="660"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549873/jobs%20photos/internships/PuppyDay.jpg">
        </div>

        <div class="item" data-w="3264" data-h="2448"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549873/jobs%20photos/internships/cookiess.jpg">
        </div>

        <div class="item" data-w="900" data-h="1200"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549871/jobs%20photos/internships/GelatoIAN.jpg">
        </div>

        <div class="item" data-w="407" data-h="724"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1496685036/jobs%20photos/Intern_Night.jpg">
        </div>

        <div class="item" data-w="1632" data-h="1224"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549872/jobs%20photos/internships/BoatCruise.jpg">
        </div>

        <div class="item" data-w="3264" data-h="2448"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549873/jobs%20photos/internships/IBooth.jpg">
        </div>

        <div class="item" data-w="960" data-h="1280"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549871/jobs%20photos/internships/BoatCruis2.jpg">
        </div>

        <div class="item" data-w="1632" data-h="1224"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549871/jobs%20photos/internships/IAN.jpg">
        </div>


        <div class="item" data-w="2320" data-h="3088"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549872/jobs%20photos/internships/Alexis.jpg">
        </div>

        <div class="item" data-w="720" data-h="1280"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549873/jobs%20photos/internships/PlantDay.jpg">
        </div>

        <div class="item" data-w="1080" data-h="1920"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549871/jobs%20photos/internships/Xmas.jpg">
        </div>

        <div class="item" data-w="1242" data-h="2208"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549871/jobs%20photos/internships/ChocolateFoutain.jpg">
        </div>

        <div class="item" data-w="940" data-h="1190"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549872/jobs%20photos/internships/Logo.jpg">
        </div>

        <div class="item" data-w="1632" data-h="1224"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549872/jobs%20photos/internships/IANZebra.jpg">
        </div>

        <div class="item" data-w="1280" data-h="1032"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549871/jobs%20photos/internships/IANplay.jpg">
        </div>

        <div class="item" data-w="800" data-h="993"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549872/jobs%20photos/internships/OfficesiteChelseapeirs.jpg">
        </div>

        <div class="item" data-w="800" data-h="1009"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549873/jobs%20photos/internships/IANLGM.jpg">
        </div>

        <div class="item" data-w="1160" data-h="622"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549874/jobs%20photos/internships/UnionSquareView.jpg">
        </div>

        <div class="item" data-w="640" data-h="852"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549874/jobs%20photos/internships/Jamesturns21.jpg">
        </div>

        <div class="item" data-w="852" data-h="640"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549874/jobs%20photos/internships/Boatcruise3.jpg">
        </div>

        <div class="item" data-w="1200" data-h="900"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549874/jobs%20photos/internships/Fooddrive.jpg">
        </div>

        <div class="item" data-w="1224" data-h="1632"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549874/jobs%20photos/internships/Cereal.jpg">
        </div>

        <div class="item" data-w="2016" data-h="1512"><img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1481074006/website/graphics/pixel.gif"
            data-src="https://res.cloudinary.com/updater-marketing/image/upload/c_scale,f_auto,q_auto,w_800/v1497549872/jobs%20photos/internships/NewOffice.jpg">
        </div>
      </div>

    </section>
  </section>
</section>

<squarespace:script src="jquery-3.5.1.min.js" combo="true" />
<squarespace:script src="jquery.flex-images.js" combo="true" />
<squarespace:script src="featherlight.min.js" combo="true" />
<squarespace:script src="upd-ui-bits_20_07.min.js" combo="true" />
<script>
  function init() {
    initNav(); // Initialize mobile menu on smaller viewport
    $('#behindthescenes2').flexImages({
      rowHeight: 400,
      truncate: 1
    });
  }
  window.addEventListener('DOMContentLoaded', init);
</script>
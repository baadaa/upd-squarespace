<main class="the-app">
  <section class="mover-app">
    <div class="breadcrumb-2018">
      <nav class="container-2018">
        <ul>
          <li>How It Works</li>
          <li><a href="/moving-app/">For People Moving</a></li>
        </ul>
      </nav>
    </div>
    <section class="mover-app--hero">
      <section class="mover-app--hero-wrapper container-2018">
        <h4>America's #1 Moving App</h4>
        <h1>The Smartest Move<br>You'll Ever Make</h1>
      </section>
    </section>
    <section class="home-2018--get-access mover-app--cards-area" id="mover-app-cards">
      <div class="container-2018">

        <h2>Start Using Updater Today, for Free</h2>
        <h3>Two ways to access</h3>
        <div class="home-2018--cards-wrapper">
          <div class="home-2018--card upd-lite">
            <div class="flipper home-2018--card-lite">
              <div class="front">
                <img
                  src="https://res.cloudinary.com/updater-marketing/image/upload/c_pad,h_100,w_100/v1546278642/website/2018-revamp-assets/icons/for-everyone.svg"
                  alt="Open" style="width: 70px;" class="card-icon">
                <h4>For Everyone</h4>
                <hr />
                <h6>Free moving tools available to all.</h6>
                <ul class="included">
                  <li>Connect TV and internet</li>
                  <li>Get home security</li>
                  <li>Find a moving company </li>
                  <li>Rent a moving truck</li>
                  <li>Download moving checklist</li>
                  <li>Estimate your moving costs</li>
                  <li>Find free boxes</li>
                  <li>And more!</li>
                </ul>
                <div class="home-2018--card-cta">
                  <a href="/free-moving-tools" class="card-cta no-flip" id="uninvited-flipstart-app-page">Get
                    started</a>
                </div>
              </div>
            </div>
          </div>
          <div class="home-2018--card upd-full">
            <div class="flipper home-2018--card-full">
              <div class="front">
                <img
                  src="https://res.cloudinary.com/updater-marketing/image/upload/v1545675711/website/2018-revamp-assets/icons/invite-only.svg"
                  style="width: 60px; margin-bottom: -5px;" alt="Invite Only" class="card-icon">
                <h4>Invite Only</h4>
                <hr />
                <h6>Invite-only access to all premium features.</h6>
                <ul class="included">
                  <li>Connect TV and internet</li>
                  <li>Get home security</li>
                  <li>Secure insurance </li>
                  <li>Reserve a moving company</li>
                  <li>Connect utilities</li>
                  <li>Forward mail </li>
                  <li>Update accounts</li>
                  <li>Claim exclusive deals</li>
                  <li>And more custom features</li>
                </ul>
                <div class="home-2018--card-cta">
                  <a href="#" class="card-cta upd-full" id="invite-only-flipstart-app-page">Learn more</a>
                </div>
              </div>
              <div class="back">
                <span class="card-close upd-full">X</span>
                <img
                  src="https://res.cloudinary.com/updater-marketing/image/upload/v1545675711/website/2018-revamp-assets/icons/invite-only.svg"
                  style="width: 40px;" alt="Invite Only" class="card-icon">
                <h4>Move Smart: Tackle everything you need in one place.</h4>
                <p>To access the platform, one of our real estate partners must send you a personalized invite.</p>
                <p>Get invited by your:</p>
                <div class="ul-wrapper">
                  <ul class="card-back">
                    <li>Real estate agent or brokerage</li>
                    <li>Apartment community</li>
                    <li>Mortgage or title rep</li>
                    <li>Moving company</li>
                    <li>University</li>
                  </ul>
                </div>
                <a class="home-2018--card-call-btn" id="request-invite-app-page" style="margin-top: 20px;"
                  href="mailto:?subject=Do%20you%20use%20Updater%3F&body=Hi%0ADo%20you%20use%20Updater%3F?%20I%20hear%20it%20is%20an%20awesome%20app%20to%20help%20me%20move%20but%20invite%20only.%20Can%20you%20help%3F%20Thanks">Ask
                  for an invite</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="mover-app--testimonial container-2018">
      <section class="mover-app--testimonial-slider">
        <h2>People Love Updater</h2>
        <div class="mover-app--carousel" data-flickity='{ "wrapAround": true }'>
          <div class="mover-app--carousel-inner">

            <div class="right-side">
              <span class="testimonial-words">First move without back pain. Updater made it so simple to choose a
                trustworthy moving company and lock in a great rate that I couldn’t resist.</span>
              <div class="testimonial-by">
                <img class="testimonial-headshot"
                  src="https://res.cloudinary.com/updater-marketing/image/upload/v1545877801/website/2018-revamp-assets/photos/people/josh.jpg">
                <div>
                  <span class="testimonial-name">Josh D.</span>
                  <span class="testimonial-location">Brooklyn to Cambridge</span>
                </div>
              </div>
            </div>
          </div>
          <div class="mover-app--carousel-inner">
            <div class="right-side">
              <span class="testimonial-words">I work from home so not having Wi-Fi for a week was not an option. I
                booked my service through Updater and had it installed by move-in day at a BIG discount. You guys
                rock!</span>
              <div class="testimonial-by">
                <img class="testimonial-headshot"
                  src="https://res.cloudinary.com/updater-marketing/image/upload/v1545877801/website/2018-revamp-assets/photos/people/bryan.jpg">
                <div>
                  <span class="testimonial-name">Bryan J.</span>
                  <span class="testimonial-location">Boston to Cambridge</span>
                </div>
              </div>
            </div>
          </div>
          <div class="mover-app--carousel-inner">
            <div class="right-side">
              <span class="testimonial-words">As a busy mom I don't have time to spend hours on the phone updating all
                my accounts. Updater made the whole process easy and fast!</span>
              <div class="testimonial-by">
                <img class="testimonial-headshot"
                  src="https://res.cloudinary.com/updater-marketing/image/upload/v1545877801/website/2018-revamp-assets/photos/people/allison.jpg">
                <div>
                  <span class="testimonial-name">Allison J.</span>
                  <span class="testimonial-location">Orlando to Orlando</span>
                </div>
              </div>
            </div>
          </div>
          <div class="mover-app--carousel-inner">

            <div class="right-side">
              <span class="testimonial-words">Just forwarded my mail in 30 seconds flat, for free. Plus, Updater's
                support team was incredibly helpful and efficient in answering all of my questions.</span>
              <div class="testimonial-by">
                <img class="testimonial-headshot"
                  src="https://res.cloudinary.com/updater-marketing/image/upload/v1545877801/website/2018-revamp-assets/photos/people/melissa.jpg">
                <div>
                  <span class="testimonial-name">Melissa T.</span>
                  <span class="testimonial-location">Brooklyn to Queens</span>
                </div>
              </div>
            </div>
          </div>
          <div class="mover-app--carousel-inner">
            <div class="right-side">
              <span class="testimonial-words">It streamlined all the annoying logistics of moving, like hooking up my
                utilities online.</span>
              <div class="testimonial-by">
                <img class="testimonial-headshot"
                  src="https://res.cloudinary.com/updater-marketing/image/upload/v1545877801/website/2018-revamp-assets/photos/people/jessica.jpg">
                <div>
                  <span class="testimonial-name">Jessica S.</span>
                  <span class="testimonial-location">Pasadena to Los Angeles</span>
                </div>
              </div>
            </div>
          </div>
          <div class="mover-app--carousel-inner">
            <div class="right-side">
              <span class="testimonial-words">I've moved dozens of times and purchasing home insurance has never been
                this painless. Spoke to a great agent who got me the coverage I needed within my budget without any
                hassle.</span>
              <div class="testimonial-by">
                <img class="testimonial-headshot"
                  src="https://res.cloudinary.com/updater-marketing/image/upload/v1546464777/website/2018-revamp-assets/photos/people/dan.jpg">
                <div>
                  <span class="testimonial-name">Dan P.</span>
                  <span class="testimonial-location">Charlotte to Charlotte</span>
                </div>
              </div>
            </div>
          </div>
        </div>

      </section>
      <section class="mover-app--testimonial-sneakpeek">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1527792670/animations/iphone-app-highlight_2.gif"
          alt="">
      </section>
    </section>
    <section class="mover-app--intro">
      <h2>Time to Take Back Your Life</h2>
      <h4>You have more important things to do than stress over moving</h4>
      <section class="mover-app--intro-benefits container-2018">
        <h5 class="counter-trigger">Updater saves you</h5>
        <div class="mover-app--intro-grid">
          <div class="mover-app--intro-grid-item">
            <h4>Time</h4>
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1546238408/website/2018-revamp-assets/icons/app-benefits/benefits-time-circle-outline-ko.svg"
              alt="Time">
            <h6><span id="count-1">9</span></h6>
            hours<br>saved
          </div>
          <div class="mover-app--intro-grid-item">
            <h4>Money</h4>
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1546238408/website/2018-revamp-assets/icons/app-benefits/benefits-discount-circle-outline-ko.svg"
              alt="Discounts">
            <h6>$<span id="count-5">500</span></h6>
            in exclusive<br>discounts
          </div>
          <div class="mover-app--intro-grid-item">
            <h4>Effort</h4>
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1546238408/website/2018-revamp-assets/icons/app-benefits/benefits-call-email-circle-outline-ko.svg"
              alt="Emails and Calls">
            <h6><span id="count-3">21</span></h6>
            calls/emails<br>eliminated
          </div>

        </div>
      </section>

    </section>
    <section class="mover-app--features">
      <h2>Key Features</h2>
      <section class="mover-app--features-list container-2018">
        <ul>
          <li id="feature-tier1" class="active">
            Moving prep & account updates
          </li>
          <li id="feature-tier2">
            Setting up
            services
          </li>
          <li id="feature-tier3">
            Local community
            & exclusive deals
          </li>
        </ul>
        <section class="mover-app--features-content feature-tier1 active">
          <div class="mover-app--features-content-item">
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1545074022/website/2018-revamp-assets/icons/app-features/icon-app-feature-reserve-a-mover.svg"
              class="feature-mover">
            Reserve a<br>moving company
          </div>
          <div class="mover-app--features-content-item">
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1545073998/website/2018-revamp-assets/icons/app-features/icon-app-feature-diy-moving.svg"
              class="feature-diy">
            Book a truck<br>& labor
          </div>
          <div class="mover-app--features-content-item">
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1545074018/website/2018-revamp-assets/icons/app-features/icon-app-feature-mailforwarding.svg"
              class="feature-mail">
            Forward<br>mail
          </div>
          <div class="mover-app--features-content-item">
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1545073995/website/2018-revamp-assets/icons/app-features/icon-app-feature-account-update.svg"
              class="feature-account">
            Update<br>accounts
          </div>
          <div class="mover-app--features-content-item">
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1545074020/website/2018-revamp-assets/icons/app-features/icon-app-feature-moving-announcement.svg"
              class="feature-address">
            Share<br>
            new address
          </div>
        </section>
        <section class="mover-app--features-content feature-tier2">
          <div class="mover-app--features-content-item">
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1545074025/website/2018-revamp-assets/icons/app-features/icon-app-feature-tv-internet.svg"
              class="feature-tv">
            Connect TV<br>& internet
          </div>
          <div class="mover-app--features-content-item">
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1545074001/website/2018-revamp-assets/icons/app-features/icon-app-feature-insurance.svg"
              class="feature-insurance">
            Secure<br>insurance
          </div>
          <div class="mover-app--features-content-item">
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1545074028/website/2018-revamp-assets/icons/app-features/icon-app-feature-utilities.svg"
              class="feature-utility">
            Connect<br>utilities
          </div>
          <div class="mover-app--features-content-item">
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1546403994/website/2018-revamp-assets/icons/app-features/icon-app-feature-home-security.svg"
              class="feature-security">
            Set up<br>home security
          </div>
        </section>
        <section class="mover-app--features-content feature-tier3">
          <div class="mover-app--features-content-item">
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1546266552/website/2018-revamp-assets/icons/app-features/icon-app-feature-resident-portal.svg"
              class="feature-portal">
            Access<br>resident portal
          </div>
          <div class="mover-app--features-content-item">
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1545074004/website/2018-revamp-assets/icons/app-features/icon-app-feature-local-deals.svg"
              class="feature-deals">
            Claim<br>exclusive deals
          </div>
          <div class="mover-app--features-content-item">
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1546266545/website/2018-revamp-assets/icons/app-features/icon-app-feature-preferred-provider.svg"
              class="feature-preferred">
            Explore<br>preferred providers
          </div>
        </section>
      </section>
      <a class="blue-btn-2018" href="#">Get Updater</a>
    </section>
    <section class="blog-preview-items app-page">
      <div class="container-2018" style="text-align: center;">
        <h2>Product News</h2>
        <div class="blog-preview-2018" data-flickity='{ "wrapAround": true }'>
          <squarespace:query collection="product-news" limit="6">
            {.repeated section items}
            <div class="blog-preview-item-2018">
              <a href="{fullUrl}" class="thumb">
                <img class="wo_cloudinary__blog-preview" src="{assetUrl}?format=500w" alt="{title}">
              </a>
              <span class="blog-list--item-date">
                <time datetime="{addedOn|date %F}">{addedOn|date %B %d, %Y}</time>
              </span>
              <h4 class="title">
                <a href="{fullUrl}">{title}</a>
              </h4>
            </div>
            {.end}
          </squarespace:query>
        </div>
        <a class="blue-btn-outline-2018" href="/product-news/">Browse more product news</a>
      </div>
    </section>
    <section class="mover-app--big-cta">
      <a href="#">Get Updater</a>
    </section>
  </section>
</main>

<squarespace:script src="jquery-3.5.1.min.js" combo="true" />
<squarespace:script src="flickity.pkgd.min.js" combo="true" />
<squarespace:script src="countUp.min.js" combo="true" />
<squarespace:script src="upd-ui-bits_20_07.min.js" combo="true" />
<script>
  function init() {
    var counters = [{
      id: 'count-1',
      number: 9,
      duration: 3
    }, {
      id: 'count-3',
      number: 21,
      duration: 5
    }, {
      id: 'count-5',
      number: 500,
      duration: 3
    }];


    currentNav('How it works'); // Set current item in top nav
    initNav(); // Initialize mobile menu on smaller viewport
    // blogListHandler(); // Blog list-specific DOM adjustments
    triggerCounting(counters);
    toggleReFeatures();
    // cardFlip();
    jumpToCardInit();
    // // cta_top_and_bottom_nav('moving-support', 'no-top');
  }

  function jumpToCardInit() {
    function jumpToCards(e) {
      e.preventDefault();
      var targetElem = document.querySelector('#mover-app-cards');

      window.scrollTo({
        top: targetElem.offsetTop - 120,
        behavior: 'smooth'
      })
    };
    document.querySelector('.mover-app--big-cta > a').addEventListener('click', jumpToCards);
    document.querySelector('.mover-app--features > a').addEventListener('click', jumpToCards);
  }

  function triggerCounting(counters) {
    $(window).scroll(function () {
      var hT = $('.counter-trigger').offset().top,
        hH = $('.counter-trigger').outerHeight(),
        wH = $(window).height(),
        wS = $(this).scrollTop();
      if (wS > (hT + hH - wH) && (hT > wS) && (wS + wH > hT + hH)) {
        countUp(counters);
      }
    });
  }

  function countUp(counters) {
    var options = {
      useEasing: true,
      useGrouping: true,
      separator: ',',
      decimal: '.',
    };

    counters.forEach(function (counter) {
      // console.log(counter);
      var counting = new CountUp(counter.id, 0, counter.number, 0, counter.duration, options)
      if (!counting.error) {
        counting.start();
      } else {
        console.error(counting.error);
      }
    });
    $(window).unbind('scroll'); // trigger only once.
  }

  window.addEventListener('DOMContentLoaded', init);
</script>
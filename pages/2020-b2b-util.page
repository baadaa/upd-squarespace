<section class="b2b-2020 util">
  <section class="hero-area">
    <squarespace:block-field id="b2b-util--header-area" />
    <section class="hero-content-wrapper container-2018">
      <section class="text-element">
        <h1>
          Consumers expect <em>more</em> from all brands <span style="font-weight: 200;">(that means you, too,
            utilities!)</span>
        </h1>
        <h2>
          The utility industry is shifting rapidly, and consumers expect a
          seamless digital experience to manage their home.
        </h2>
        <button>Partner with us</button>
      </section>
      <style>
        .featherlight.featherlight-iframe .featherlight-content {
          height: 0;
          padding-top: 56.25%;
          width: 100%;
          margin-left: auto;
          margin-right: auto;
        }
      </style>
      <section class="image-element">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1583863327/website/2020-b2b-relaunch/hero-images/shutterstock_76073197_r4B.png"
          alt="Future of utilities" />
        <a href="https://www.youtube.com/embed/USBfNllQX-U?rel=0&amp;autoplay=1" data-featherlight="iframe"
          data-featherlight-iframe-frameborder="0" data-featherlight-iframe-allow="autoplay; encrypted-media"
          data-featherlight-iframe-allowfullscreen="true"
          data-featherlight-iframe-style="position:absolute;top:0;left:0;width:100vw;height:100%;">
          <img class="video-button"
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1583875173/website/2020-b2b-relaunch/icons-and-patterns/video-prompt.png"
            alt="" />
        </a>
      </section>
    </section>
  </section>
  <section class="lead-in-takeaway">
    We provide an integrated, multi-channel moving concierge — <br />built for
    utilities, designed for customers, and <br />trusted by marketers.
  </section>
  <squarespace:block-field id="b2b-util--body-content" />
  <section class="flexible-modules">
    <h3>Leverage 6 flexible features</h3>
    <ul class="module-filter">
      <li data-filter="all" class="mixitup-control-active">All Features</li>
      <li data-filter=".call">Call Center Features</li>
      <li data-filter=".digital">Digital Features</li>
    </ul>
    <div class="cards-wrapper">
      <div class="module-card call">
        <div class="tags">
          <span class="call">Call</span>
        </div>
        <div class="image-block">
          <svg class="we-pick-calls">
            <use xlink:href="/assets/util-6-in-1-icons.svg#we-pick-calls"></use>
          </svg>
        </div>
        <div class="text-block">
          <h5>Updater picks up <br>your mover calls</h5>
          <ul>
            <li>We pick up your calls,</li>
            <li>Place service requests/orders, and</li>
            <li>Sell agreed upon products & services</li>
          </ul>
        </div>
      </div>
      <div class="module-card call">
        <div class="tags">
          <span class="call">Call</span>
        </div>
        <div class="image-block">
          <svg class="you-transfer">
            <use xlink:href="/assets/util-6-in-1-icons.svg#you-transfer"></use>
          </svg>
        </div>
        <div class="text-block">
          <h5>You transfer your <br>mover calls to Updater</h5>
          <ul>
            <li>You coordinate new service requests/orders</li>
            <li>You transfer mover & switcher calls to Updater</li>
            <li>Updater sells agreed upon products & services</li>
          </ul>
        </div>
      </div>
      <div class="module-card call digital">
        <div class="tags">
          <span class="call">Call</span>
          <span class="digital">Digital</span>
        </div>
        <div class="image-block">
          <svg class="ccui">
            <use xlink:href="/assets/util-6-in-1-icons.svg#ccui"></use>
          </svg>
        </div>
        <div class="text-block">
          <h5>Use our tech platform <br>in your call center</h5>
          <ul>
            <li>Your call center agents can place orders for TV, internet, security, and more directly into Updater's
              technology</li>
            <li>All your call center needs is an internet connection</li>
          </ul>
        </div>
      </div>
      <div class="module-card digital">
        <div class="tags">
          <span class="digital">Digital</span>
        </div>
        <div class="image-block">
          <svg class="api">
            <use xlink:href="/assets/util-6-in-1-icons.svg#api"></use>
          </svg>
        </div>
        <div class="text-block">
          <h5>Build moving features into <br>your own digital experience</h5>
          <ul>
            <li>A complete API portfolio of moving-related features, products, and services</li>
            <li>Plug moving products and services into your workflow</li>
          </ul>
        </div>
      </div>
      <div class="module-card digital">
        <div class="tags">
          <span class="digital">Digital</span>
        </div>
        <div class="image-block">
          <svg class="moving-app">
            <use xlink:href="/assets/util-6-in-1-icons.svg#moving-app"></use>
          </svg>
        </div>
        <div class="text-block">
          <h5>You offer our moving app</h5>
          <ul>
            <li>Offer a turnkey digital mover journey, branded for your company</li>
            <li>Your customers organize and complete all moving-related tasks in one place</li>
          </ul>
        </div>
      </div>
      <div class="module-card digital">
        <div class="tags">
          <span class="digital">Digital</span>
        </div>
        <div class="image-block">
          <svg class="user-enroll">
            <use xlink:href="/assets/util-6-in-1-icons.svg#user-enroll"></use>
          </svg>
        </div>
        <div class="text-block">
          <h5>Enroll customers in your <br>service via Updater</h5>
          <ul>
            <li>Allow Updater to place utility orders on your behalf</li>
            <li>Movers reach Updater 30+ days pre-move</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section class="unique-benefits">
    <h3>
      Benefits to make all your stakeholders smile
    </h3>
    <div class="cards-wrapper">

      <div class="concierge-card card-1">
        <h5>
          <span class="image-block">
            1
          </span>
          <span class="text-content">
            Create, enhance, and scale
          </span>
        </h5>
        <p>your concierge program, both digitally and over the phone.</p>
      </div>
      <div class="concierge-card card-2">
        <h5>
          <span class="image-block">
            2
          </span>
          <span class="text-content">
            Launch new sales channels
          </span>
        </h5>
        <p>
          to grow your ancillary revenue and reach customers before and after they
          call you.
        </p>
      </div>
      <div class="concierge-card card-3">
        <h5>
          <span class="image-block">
            3
          </span>
          <span class="text-content">
            Integrate our moving app
          </span>
        </h5>
        <p>as a critical upgrade and value-add for customers.</p>
      </div>
      <div class="concierge-card card-4">
        <h5>
          <span class="image-block">
            4
          </span>
          <span class="text-content">
            Enroll customers & prospects digitally
          </span>
        </h5>
        <p>in the products and services that you care about most.</p>
      </div>
    </div>
  </section>
  <section class="our-proven-record">
    <h3>Our proven track record</h3>
    <div class="record-card">
      <h5>Trusted by the industry leaders</h5>
      <div class="record-entry">
        <h6>
          3 of the top 5 utilities
        </h6>
        <p>
          by number of customers
        </p>
      </div>
      <div class="record-entry">
        <h6>
          All top 20
        </h6>
        <p>
          nationwide media companies
        </p>
      </div>
    </div>
    <div class="record-card">
      <h5>Powering businesses worldwide</h5>
      <div class="record-entry">
        <h6 class="smaller">
          World leader in internet&nbsp;subscriptions
        </h6>
        <p>
          We sell more broadband subscriptions than any other company in the
          world
        </p>
      </div>
      <div class="record-entry">
        <h6 class="smaller">
          50,000+ call center&nbsp;agents
        </h6>
        <p>
          Our technology enables more than 50,000 call center agents worldwide
        </p>
      </div>
    </div>
  </section>
  <section class="utility-whitepaper">
    <section class="whitepaper-wrapper container-2018">
      <section class="image-element">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1587493571/website/2020-b2b-relaunch/ebook-images/Utility-Hardcover-Book-Hand_r2.png"
          alt="whitepaper" />
      </section>
      <section class="text-and-form" id="utilities-ebook-2020">
        <h6>Utility Insights Whitepaper</h6>
        <h4>The Utility's Guide to <br />Selecting a Moving Concierge</h4>
        <h5>9 Questions to Get the Info You Need</h5>
        <p>
          Evaluate concierge service providers with Updater's recommended
          questions (and, of course, our answers to each).
        </p>
        <a
          href="https://res.cloudinary.com/updater-marketing/image/upload/v1593615529/downloads/utility/utility_guide_to_selecting_moving_concierge.pdf">
          Download ebook
        </a>
      </section>
    </section>
    <section class="business-products--form-area" id="business-products-form">
      <div class="container-2018">
        <style>
          .sfwl_form .sfwl_row {
            flex-direction: column;
            align-items: flex-start;
          }

          .sfwl_form .sfwl_row label,
          .sfwl_form div.note {
            color: #fff;
          }

          .sfwl_form .sfwl_row label,
          .sfwl_form .sfwl_row input,
          .sfwl_form .sfwl_row select,
          .sfwl_form .sfwl_row textarea {
            width: 100%;
            flex-basis: auto;
            text-align: left;
          }

          .sfwl_form .sfwl_submit {
            margin-left: auto;
            margin-right: auto;
          }
        </style>
        <h2>Partner with us</h2>
        <h6>We'll get back to you within one day.</h6>
        {@|apply sfwl-all-business-form.block}
      </div>
    </section>
  </section>
  <div class="schedule-widget untriggered" style="display: none;">
    <h5>Join us on the road</h5>
    <squarespace:block-field id="utility-event-schedule" />
    <button>Set up a time to chat</button>
  </div>
</section>

<squarespace:script src="jquery-3.5.1.min.js" combo="true" />
<squarespace:script src="featherlight.min.js" combo="true" />
<squarespace:script src="mixitup.min.js" combo="true" />
<squarespace:script src="upd-ui-bits_20_07.min.js" combo="true" />

<script>
  var init = function () {
    function pullContent() {
      var heroHeadingBlock = document.querySelector(
        ".hero-area .text-element h1"
      );
      var heroDescriptionBlock = document.querySelector(
        ".hero-area .text-element h2"
      );
      var heroCtaBlock = document.querySelector(
        ".hero-area .text-element button"
      );
      heroHeadingBlock.innerHTML = heroText.heading;
      heroDescriptionBlock.innerHTML = heroText.description;
      heroCtaBlock.innerHTML = heroText.buttonText;

      var leadInCopyBlock = document.querySelector(".lead-in-takeaway");
      leadInCopyBlock.innerHTML = leadInCopy;

      var conciergeSectionHeading = document.querySelector(
        ".smarter-concierge h3"
      );
      var conciergeCard1_heading = document.querySelector(
        ".concierge-card.card-1 h5 .text-content"
      );
      var conciergeCard1_body = document.querySelector(
        ".concierge-card.card-1 p"
      );
      var conciergeCard2_heading = document.querySelector(
        ".concierge-card.card-2 h5 .text-content"
      );
      var conciergeCard2_body = document.querySelector(
        ".concierge-card.card-2 p"
      );
      var conciergeCard3_heading = document.querySelector(
        ".concierge-card.card-3 h5 .text-content"
      );
      var conciergeCard3_body = document.querySelector(
        ".concierge-card.card-3 p"
      );
      var conciergeCard4_heading = document.querySelector(
        ".concierge-card.card-4 h5 .text-content"
      );
      var conciergeCard4_body = document.querySelector(
        ".concierge-card.card-4 p"
      );

      conciergeSectionHeading.innerHTML = conciergeSection.title;
      conciergeCard1_heading.innerHTML = conciergeSection.card_1.heading;
      conciergeCard1_body.innerHTML = conciergeSection.card_1.description;
      conciergeCard2_heading.innerHTML = conciergeSection.card_2.heading;
      conciergeCard2_body.innerHTML = conciergeSection.card_2.description;
      conciergeCard3_heading.innerHTML = conciergeSection.card_3.heading;
      conciergeCard3_body.innerHTML = conciergeSection.card_3.description;
      conciergeCard4_heading.innerHTML = conciergeSection.card_4.heading;
      conciergeCard4_body.innerHTML = conciergeSection.card_4.description;

      var trackRecordHeading = document.querySelector(".our-proven-record h3");
      var trackCard1Heading = document.querySelector(
        ".our-proven-record .record-card:nth-of-type(1) h5"
      );
      var trackCard1_entry1_big = document.querySelector(
        ".our-proven-record .record-card:nth-of-type(1) .record-entry:nth-of-type(1) h6"
      );
      var trackCard1_entry1_small = document.querySelector(
        ".our-proven-record .record-card:nth-of-type(1) .record-entry:nth-of-type(1) p"
      );
      var trackCard1_entry2_big = document.querySelector(
        ".our-proven-record .record-card:nth-of-type(1) .record-entry:nth-of-type(2) h6"
      );
      var trackCard1_entry2_small = document.querySelector(
        ".our-proven-record .record-card:nth-of-type(1) .record-entry:nth-of-type(2) p"
      );
      var trackCard2Heading = document.querySelector(
        ".our-proven-record .record-card:nth-of-type(2) h5"
      );
      var trackCard2_entry1_big = document.querySelector(
        ".our-proven-record .record-card:nth-of-type(2) .record-entry:nth-of-type(1) h6"
      );
      var trackCard2_entry1_small = document.querySelector(
        ".our-proven-record .record-card:nth-of-type(2) .record-entry:nth-of-type(1) p"
      );
      var trackCard2_entry2_big = document.querySelector(
        ".our-proven-record .record-card:nth-of-type(2) .record-entry:nth-of-type(2) h6"
      );
      var trackCard2_entry2_small = document.querySelector(
        ".our-proven-record .record-card:nth-of-type(2) .record-entry:nth-of-type(2) p"
      );

      trackRecordHeading.innerHTML = trackRecordSection.title;
      trackCard1Heading.innerHTML = trackRecordSection.card_1.title;
      trackCard1_entry1_big.innerHTML = trackRecordSection.card_1.bigText_1;
      trackCard1_entry1_small.innerHTML = trackRecordSection.card_1.smallText_1;
      trackCard1_entry2_big.innerHTML = trackRecordSection.card_1.bigText_2;
      trackCard1_entry2_small.innerHTML = trackRecordSection.card_1.smallText_2;
      trackCard2Heading.innerHTML = trackRecordSection.card_2.title;
      trackCard2_entry1_big.innerHTML = trackRecordSection.card_2.bigText_1;
      trackCard2_entry1_small.innerHTML = trackRecordSection.card_2.smallText_1;
      trackCard2_entry2_big.innerHTML = trackRecordSection.card_2.bigText_2;
      trackCard2_entry2_small.innerHTML = trackRecordSection.card_2.smallText_2;

      var servicesHeading = document.querySelector(".service-highlight h3");
      var serviceCard1Heading = document.querySelector(
        ".service-highlight .highlight-card:nth-of-type(1) h5 .text-content"
      );
      var serviceCard1Body = document.querySelector(
        ".service-highlight .highlight-card:nth-of-type(1) p"
      );
      var serviceCard2Heading = document.querySelector(
        ".service-highlight .highlight-card:nth-of-type(2) h5 .text-content"
      );
      var serviceCard2Body = document.querySelector(
        ".service-highlight .highlight-card:nth-of-type(2) p"
      );
      var serviceCard3Heading = document.querySelector(
        ".service-highlight .highlight-card:nth-of-type(3) h5 .text-content"
      );
      var serviceCard3Body = document.querySelector(
        ".service-highlight .highlight-card:nth-of-type(3) p"
      );
      var serviceCard4Heading = document.querySelector(
        ".service-highlight .highlight-card:nth-of-type(4) h5 .text-content"
      );
      var serviceCard4Body = document.querySelector(
        ".service-highlight .highlight-card:nth-of-type(4) p"
      );
      var serviceCard5Heading = document.querySelector(
        ".service-highlight .highlight-card:nth-of-type(5) h5 .text-content"
      );
      var serviceCard5Body = document.querySelector(
        ".service-highlight .highlight-card:nth-of-type(5) p"
      );
      var serviceCard6Heading = document.querySelector(
        ".service-highlight .highlight-card:nth-of-type(6) h5 .text-content"
      );
      var serviceCard6Body = document.querySelector(
        ".service-highlight .highlight-card:nth-of-type(6) p"
      );

      servicesHeading.innerHTML = serviceHighlightSection.title;
      serviceCard1Heading.innerHTML = serviceHighlightSection.card_1.title;
      serviceCard1Body.innerHTML = serviceHighlightSection.card_1.body;
      serviceCard2Heading.innerHTML = serviceHighlightSection.card_2.title;
      serviceCard2Body.innerHTML = serviceHighlightSection.card_2.body;
      serviceCard3Heading.innerHTML = serviceHighlightSection.card_3.title;
      serviceCard3Body.innerHTML = serviceHighlightSection.card_3.body;
      serviceCard4Heading.innerHTML = serviceHighlightSection.card_4.title;
      serviceCard4Body.innerHTML = serviceHighlightSection.card_4.body;
      serviceCard5Heading.innerHTML = serviceHighlightSection.card_5.title;
      serviceCard5Body.innerHTML = serviceHighlightSection.card_5.body;
      serviceCard6Heading.innerHTML = serviceHighlightSection.card_6.title;
      serviceCard6Body.innerHTML = serviceHighlightSection.card_6.body;

      var ebookSectionHeading = document.querySelector(
        ".utility-whitepaper .text-and-form h6"
      );
      var ebookTitle = document.querySelector(
        ".utility-whitepaper .text-and-form h4"
      );
      var ebookSubheading = document.querySelector(
        ".utility-whitepaper .text-and-form h5"
      );
      var ebookSummary = document.querySelector(
        ".utility-whitepaper .text-and-form p"
      );

      ebookSectionHeading.innerHTML = ebookSection.sectionTitle;
      ebookTitle.innerHTML = ebookSection.ebookTitle;
      ebookSubheading.innerHTML = ebookSection.ebookSubheading;
      ebookSummary.innerHTML = ebookSection.ebookSummary;
    }

    function initScheduler() {
      function checkPosition(e) {
        if (window.scrollY <= 150) {
          scheduleWidget.classList.add("untriggered");
        } else {
          scheduleWidget.classList.remove("untriggered");
        }
        if (
          window.scrollY >
          document.body.clientHeight -
          (footerElem.clientHeight * 3 + formArea.clientHeight)
        ) {
          scheduleWidget.classList.add("untriggered");
        }
      }

      function checkWidth() {
        if (document.body.clientWidth <= 600) {
          scheduleWidget.classList.add("collapsed");
        }
      }

      function toggleWidget() {
        scheduleWidget.classList.toggle("collapsed");
      }

      function jumpToForm() {
        checkPosition();
        window.scrollTo({
          top: formArea.offsetTop,
          behavior: "smooth",
        });
      }

      var scheduleWidget = document.querySelector(".schedule-widget");
      var schedulerBtn = scheduleWidget.querySelector("button");
      var heroAreaBtn = document.querySelector(".hero-area button");
      var widgetHead = scheduleWidget.querySelector("h5");
      var openPrompt = scheduleWidget.querySelector(".open-prompt");
      var footerElem = document.querySelector("footer.revamp-2018-footer");
      var formArea = document.querySelector("#business-products-form");

      document.addEventListener("scroll", checkPosition);
      window.addEventListener("resize", checkWidth);
      widgetHead.addEventListener("click", toggleWidget);
      schedulerBtn.addEventListener("click", jumpToForm);
      heroAreaBtn.addEventListener("click", jumpToForm);

      function initiallSetup() {
        if (window.innerWidth <= 600) {
          scheduleWidget.classList.add("collapsed");
        }
        checkPosition();
      }
      initiallSetup();
    }

    function initMixItUp() {
      var mixer = mixitup('.cards-wrapper', {
        selectors: {
          target: '.module-card',
        },
        animation: {
          effects: 'fade scale translateZ(-100px)',
          duration: 600,
          clampHeight: false
        }
      });
    }
    initNav();
    currentNav("I'm a business");
    // pullContent();
    initScheduler();
    initMixItUp();
  };
  window.addEventListener('DOMContentLoaded', init);;
</script>
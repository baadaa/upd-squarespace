<section class="uhs-terms">
  <section class="uhs-terms--hero">
    <section class="uhs-terms--hero-wrapper container-2018">
      <h4>Updater Home Services</h4>
      <h1>Privacy and Terms</h1>
    </section>
  </section>
  <section id="content" class="uhs-terms--body">
    <nav class="terms-tab-nav container-2018">
      <ul>
        <li class="privacy-btn active">
          <svg style="height: 60px; margin-top: 7px;">
            <use xlink:href="/assets/uhs-privacy-icons.svg#privacy-policy"></use>
          </svg>
          Privacy Policy
        </li>
        <li class="ccpa-btn">
          <svg style="height: 60px;">
            <use xlink:href="/assets/uhs-privacy-icons.svg#ccpa-icon"></use>
          </svg>
          CCPA Form
        </li>
      </ul>
    </nav>
    <div class="container-2018">
      <section class="terms-content-area">
        <section class="uhs-privacy">
          <squarespace:block-field id="uhs-privacy-policy--update-date" />
          <squarespace:block-field id="uhs-privacy-policy--block" />
        </section>
        <section class="uhs-ccpa is-hidden">
          <squarespace:block-field id="uhs-ccpa--update-date" />
          <squarespace:block-field id="uhs-ccpa--block" />
        </section>
      </section>
    </div>
  </section>
</section>

<squarespace:script src="upd-ui-bits_20_07.min.js" combo="true" />
<script>
  function termsTabNav() {
    var urlString = window.location.href;
    var urlHash = urlString.split("#")[1];

    var activateTab = function (section) {
      [privacySection, ccpaSection].forEach(function (area) {
        if (!area.classList.contains("is-hidden")) {
          area.classList.add("is-hidden");
        }
      });
      [privacyBtn, ccpaBtn].forEach(function (btn) {
        btn.classList.remove("active");
      });

      switch (section) {
        case "ccpa":
          ccpaSection.classList.remove("is-hidden");
          ccpaBtn.classList.add("active");
          break;
        case "privacy":
          privacySection.classList.remove("is-hidden");
          privacyBtn.classList.add("active");
          break;
        default:
          break;
      }
    };

    var tabHandler = function (e) {
      // If clicked the currently active tab, ignore
      if (e.target.classList.contains("active")) {
        return;
      } else {
        var clicked = e.target.classList[0]; // First item in classList.
        var currentSection = "";

        // Remove active class from all the buttons
        [privacyBtn, ccpaBtn].forEach(function (btn) {
          btn.classList.remove("active");
        });

        // Make the currently clicked item 'active'
        e.target.classList.add("active");

        [privacySection, ccpaSection].forEach(function (section) {
          if (!section.classList.contains("is-hidden")) {
            section.classList.add("is-hidden");
          }
        });
        switch (clicked) {
          case "ccpa-btn":
            ccpaSection.classList.remove("is-hidden");
            break;
          case "privacy-btn":
            privacySection.classList.remove("is-hidden");
            break;
          default:
            break;
        }
      }
    };

    var ccpaBtn = document.querySelector(".ccpa-btn"),
      privacyBtn = document.querySelector(".privacy-btn"),
      bpBtn = document.querySelector(".bp-btn"),
      privacySection = document.querySelector(".uhs-privacy"),
      ccpaSection = document.querySelector(".uhs-ccpa");

    // Add listeners to buttons
    [privacyBtn, ccpaBtn].forEach(function (btn) {
      btn.addEventListener("click", tabHandler);
    });
    if (
      urlHash !== undefined &&
      (urlHash === "privacy" || urlHash === "ccpa")
    ) {
      activateTab(urlHash);
    }
  }

  function init() {
    initNav(); // Initialize mobile menu on smaller viewport
    termsTabNav();
  }

  window.addEventListener('DOMContentLoaded', init);;
</script>
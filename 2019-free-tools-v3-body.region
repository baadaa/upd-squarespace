<section class="freetools-v3">

  <section class="freetools-v3--full">
    <!-- Navigation Sidebar -->
    <section class="freetools-v3--nav-full freetools-v3--desktop-only" id="freetools-v3--nav-full-id">

      <ul class="freetools-v3--nav-tabs">
        <li><a class="freetools-v3--tablink active" id="freetools-v3--defaulttab"
            data-target="freetools-v3--tab-tackle">What to Tackle First</a></li>
        <li><a class="freetools-v3--tablink" data-target="freetools-v3--tab-packing">Packing Guides</a>
        </li>
        <li><a class="freetools-v3--tablink" data-target="freetools-v3--tab-homeadvisor">Settle in Quickly</a>
        <li><a class="freetools-v3--tablink" data-target="freetools-v3--tab-updating">Update Your Info</a>
        </li>
      </ul>

      <section class="freetools-v3--nav-search">
        <h5>Looking for something specific?</h5>
        <span>Search our moving blog:</span>
        <squarespace:block-field id="searchtips" />
      </section>

    </section>

    <a class="freetools-v3--mobile-nav-btn freetools-v3--mobile-only">
      <svg id="freetools-v3--hamburger">
        <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--hamburger"></use>
      </svg>
      <svg id="freetools-v3--close">
        <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--close"></use>
      </svg>
    </a>

    <!-- Sections -->

    <!-- WHAT TO TACKLE FIRST ------------------->
    <section class="freetools-v3--main-wrapper is-on" id="freetools-v3--tab-tackle">
      <div class="freetools-v3--mobile-only freetools-v3--tab-header" id="freetools-v3--jump-tackle">What to Tackle
        First</div>

      <section class="freetools-v3--body">

        <div class="freetools-v3--card-set">

          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Set Up TV <br>and Internet</h2>
              <p>Transfer your current plan or shop for a new one.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <a id="go-tv-internet_free-moving-tools" class="freetools-v3--internet-tv-btn" href="/tv-internet"
                target="_blank" rel="noreferrer noopener">
                Get started >
              </a>
            </div>
            <svg>
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--internet"></use>
            </svg>
          </div>


          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Find a Moving Company</h2>
              <p>Get quotes from Updater-Certified moving companies.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <form id="freetools-v3--mover-search" method="get" action="/find-a-moving-company" target="_blank"
                rel="noreferrer noopener">
                <input id="freetools-v3--mover-search-field" type="text" name="locate" placeholder="ZIP or City">
                <input type="submit" value="Get a quote >" id="get-a-quote-moving-company_free-moving-tools">
              </form>
            </div>
            <svg>
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--dolly"></use>
            </svg>
          </div>

          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Rent a <br>Moving Truck</h2>
              <p>Want to go DIY? Save 10% on any Penske truck rental.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <a href="https://www.pensketruckrental.com/rental_entry.html?CID=80990119"
                id="get-started-moving-truck_free-moving-tools" target="_blank" rel="noreferrer noopener">Get started
                ></a>
            </div>
            <svg>
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--truck"></use>
            </svg>
          </div>

          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Estimate Your <br>Moving Costs</h2>
              <p>Fine-tune your moving budget with our handy calculator.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <a href="https://www.updater.com/moving-tips/moving-cost-calculator"
                id="get-started-moving-costs_free-moving-tools" target="_blank" rel="noreferrer noopener">Get started
                ></a>
            </div>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129.39 182.39">
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--calc"></use>
            </svg>
          </div>

        </div>
      </section>

      <section class="freetools-v3--sidebar freetools-v3--desktop-only">
        <svg>
          <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--checklist"></use>
        </svg>
        <span>What to Tackle First</span>
      </section>
    </section>

    <!-- PACKING GUIDES ------------------------ -->
    <section class="freetools-v3--main-wrapper" id="freetools-v3--tab-packing">
      <div class="freetools-v3--mobile-only freetools-v3--tab-header" id="freetools-v3--jump-packing">Packing Guides
      </div>

      <section class="freetools-v3--body">

        <div class="freetools-v3--card-set">

          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Your Ultimate Moving Checklist</h2>
              <p>Everything you did (and didn't) think of.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <a href="https://www.updater.com/moving-tips/the-most-epic-moving-checklist-in-the-history-of-moving-checklists"
                target="_blank" rel="noreferrer noopener">View now ></a>
            </div>
            <svg>
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--mchecklist"></use>
            </svg>
          </div>


          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Where to Find <br>Free Boxes</h2>
              <p>Trust us, you'll need 'em.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <a href="https://www.updater.com/moving-tips/top-20-places-to-find-free-moving-boxes" target="_blank"
                rel="noreferrer noopener">Learn where ></a>
            </div>
            <svg>
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--boxes"></use>
            </svg>
          </div>

          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Top 50 <br>Moving Hacks</h2>
              <p>Fifty ways to make packing a breeze.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <a href="https://www.updater.com/moving-tips/top-50-moving-hacks-of-all-time" target="_blank"
                rel="noreferrer noopener">Read now
                ></a>
            </div>
            <svg>
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--truck-dark"></use>
            </svg>
          </div>

          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>How to <br>Pack Food</h2>
              <p>What to bring, what to toss, and what to donate.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <a href="https://www.updater.com/moving-tips/your-start-to-finish-guide-for-packing-food-for-your-move"
                target="_blank" rel="noreferrer noopener">Learn how ></a>
            </div>
            <svg>
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--apple"></use>
            </svg>
          </div>
        </div>
      </section>

      <section class="freetools-v3--sidebar freetools-v3--desktop-only">
        <svg>
          <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--checklist"></use>
        </svg>
        <span>Packing Guides</span>
      </section>
    </section>
    <!-- Home Advisor -------------------- -->
    <section class="freetools-v3--main-wrapper" id="freetools-v3--tab-homeadvisor">
      <div class="freetools-v3--mobile-only freetools-v3--tab-header" id="freetools-v3--jump-contact">Settle in Quickly
      </div>

      <section class="freetools-v3--body">
        <div class="freetools-v3--card-set">

          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Schedule a <br>Home Cleaning</h2>
              <p>10% off a professional cleaning.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <a href="https://www.homeadvisor.com/direct-external-fulfill?reason=7&taskOid=40006&entry_point_id=34153392&coupon=UPDATER10"
                target="_blank" rel="noreferrer noopener">Schedule now ></a>
            </div>
            <svg class="ha-cleaning">
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#ha-cleaning"></use>
            </svg>
          </div>


          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Assemble <br>Your Furniture</h2>
              <p>10% off building your <br>beautiful new dresser.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <a style="width: 100px;"
                href="https://www.homeadvisor.com/direct-external-fulfill?reason=7&taskOid=62373&entry_point_id=34153394&coupon=UPDATER10"
                target="_blank" rel="noreferrer noopener">Book
                ></a>
            </div>
            <svg class="ha-furniture">
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#ha-furniture"></use>
            </svg>
          </div>

          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Paint a Room</h2>
              <p>10% off quality <br>painting services.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <a style="width: 175px;"
                href="https://www.homeadvisor.com/direct-external-fulfill?reason=7&taskOid=61720&entry_point_id=34153395&coupon=UPDATER10"
                target="_blank" rel="noreferrer noopener">Browse
                availability
                ></a>
            </div>
            <svg class="ha-painting">
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#ha-painting"></use>
            </svg>
          </div>

          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Hang Your TV</h2>
              <p>10% off a TV mounting <br>professional.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <a href="https://www.homeadvisor.com/direct-external-fulfill?reason=7&taskOid=46380&entry_point_id=34153393&coupon=UPDATER10"
                target="_blank" rel="noreferrer noopener">Schedule now ></a>
            </div>
            <svg class="ha-tv-mounting">
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#ha-tv-mounting"></use>
            </svg>
          </div>
        </div>

      </section>

      <section class="freetools-v3--sidebar freetools-v3--desktop-only">
        <svg>
          <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--checklist"></use>
        </svg>
        <span>Settle in Quickly</span>
      </section>
    </section>
    <!-- UPDATING YOUR INFO ------------------------- -->
    <section class="freetools-v3--main-wrapper" id="freetools-v3--tab-updating">
      <div class="freetools-v3--mobile-only freetools-v3--tab-header" id="freetools-v3--jump-updating">Update Your Info
      </div>

      <section class="freetools-v3--body">

        <div class="freetools-v3--card-set">

          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Update Your <br>Driver's License</h2>
              <p>All the info you'll need, in one place.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <div class="freetools-v3--state-select">
                <select id="freetools-v3--state-select-field">
                  <option value>Select your state</option>
                  <option value="AL">Alabama</option>
                  <option value="AK">Alaska</option>
                  <option value="AZ">Arizona</option>
                  <option value="AR">Arkansas</option>
                  <option value="CA">California</option>
                  <option value="CO">Colorado</option>
                  <option value="CT">Connecticut</option>
                  <option value="DE">Delaware</option>
                  <option value="DC">District of Columbia</option>
                  <option value="FL">Florida</option>
                  <option value="GA">Georgia</option>
                  <option value="HI">Hawaii</option>
                  <option value="ID">Idaho</option>
                  <option value="IL">Illinois</option>
                  <option value="IN">Indiana</option>
                  <option value="IA">Iowa</option>
                  <option value="KS">Kansas</option>
                  <option value="KY">Kentucky</option>
                  <option value="LA">Louisiana</option>
                  <option value="ME">Maine</option>
                  <option value="MD">Maryland</option>
                  <option value="MA">Massachusetts</option>
                  <option value="MI">Michigan</option>
                  <option value="MN">Minnesota</option>
                  <option value="MS">Mississippi</option>
                  <option value="MO">Missouri</option>
                  <option value="MT">Montana</option>
                  <option value="NE">Nebraska</option>
                  <option value="NV">nevada</option>
                  <option value="NH">New Hampshire</option>
                  <option value="NJ">New Jersey</option>
                  <option value="NM">New Mexico</option>
                  <option value="NY">New York</option>
                  <option value="NC">North Carolina</option>
                  <option value="ND">North Dakota</option>
                  <option value="OH">Ohio</option>
                  <option value="OK">Oklahoma</option>
                  <option value="OR">Oregon</option>
                  <option value="PA">Pennsylvania</option>
                  <option value="RI">Rhode Island</option>
                  <option value="SC">South Carolina</option>
                  <option value="SD">South Dakota</option>
                  <option value="TN">Tennessee</option>
                  <option value="TX">Texas</option>
                  <option value="UT">Utah</option>
                  <option value="VT">Vermont</option>
                  <option value="VA">Virginia</option>
                  <option value="WA">Washington</option>
                  <option value="WV">West Virginia</option>
                  <option value="WI">Wisconsin</option>
                  <option value="WY">Wyoming</option>
                </select>
                <a id="freetools-v3--state-select-btn">Go</a>
              </div>
              <svg>
                <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--license"></use>
              </svg>
            </div>
          </div>

          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Transfer Your <br>Utilities</h2>
              <p>Get it done early and arrive at your new place stress-free.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <a href="https://www.updater.com/moving-tips/documents-youll-need-to-start-stop-or-transfer-utility-service"
                target="_blank" rel="noreferrer noopener">View how ></a>
            </div>
            <svg>
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--bulb"></use>
            </svg>
          </div>

          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Who to Notify <br>When You Move</h2>
              <p>Tell the whole world! But start here first.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <a href="https://www.updater.com/moving-tips/who-to-notify-when-you-move-or-change-your-address"
                target="_blank" rel="noreferrer noopener">View list ></a>
            </div>
            <svg xmlns="http://www.w3.org/2000/svg">
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--megaphone"></use>
            </svg>
          </div>

          <div class="freetools-v3--card">
            <div class="freetools-v3--card-text">
              <h2>Update Your Voter Registration</h2>
              <p>It's your civic duty, and it's easy. We're here to help.</p>
            </div>
            <div class="freetools-v3--card-cta">
              <a href="https://www.updater.com/moving-tips/moving-how-to-update-your-voter-registration-information-in-every-state"
                target="_blank" rel="noreferrer noopener">Learn how ></a>
            </div>
            <svg>
              <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--checklist-g"></use>
            </svg>
          </div>
        </div>
      </section>

      <section class="freetools-v3--sidebar freetools-v3--desktop-only">
        <svg>
          <use xlink:href="/assets/free-tools-v3-svg-icon-defs.svg#free-tools-v3--checklist"></use>
        </svg>
        <span>Update Your Info</span>
      </section>
    </section>

  </section>


  <section class="blog-preview-items">
    <div class="container-2018" style="text-align: center;">
      <h2>More Moving Tips, Tricks, and Advice</h2>
      <div class="blog-preview-2018">
        <squarespace:query collection="moving-tips" limit="6">
          {.repeated section items}
          <div class="blog-preview-item-2018">
            <a href="{fullUrl}" class="thumb">
              <img class="wo_cloudinary__blog-preview" src="{assetUrl}?format=500w" alt="{title}">
            </a>
            <a href="{fullUrl}">
              <h4 class="title">
                {title}
              </h4>
            </a>
          </div>
          {.end}
        </squarespace:query>
      </div>
      <a class="blue-btn-outline-2018" href="/moving-tips">Browse more tips</a>
    </div>
  </section>
</section>

<squarespace:script src="flickity.pkgd.min.js" combo="true" />
<squarespace:script src="upd-ui-bits_20_07.min.js" combo="true" />

<script>
  function handleTab(e) {
    var currentTab = e.target;
    var targetTab = e.target.dataset.target;
    // console.log(currentTab, targetTab);
    document.querySelectorAll('.freetools-v3--main-wrapper').forEach(function (content) {
      if (content.id !== targetTab) {
        content.classList.remove('is-on');
      } else {
        content.classList.add('is-on');
      }
    });

    document.querySelectorAll('.freetools-v3--tablink').forEach(function (tab) {
      if (tab !== currentTab) {
        tab.classList.remove('active');
      } else {
        tab.classList.add('active');
      }
    });

    if (window.innerWidth <= 550) {
      window.scrollTo({
        top: document.getElementById(targetTab).offsetTop - 70,
        behavior: 'smooth'
      });
      toggleMobileNav();

    }
  }

  function freetoolsTabInit() {

    document.querySelectorAll('.freetools-v3--tablink').forEach(function (tab) {
      tab.addEventListener('click', handleTab);
    })
    document.querySelector('.freetools-v3--mobile-nav-btn').addEventListener('click', toggleMobileNav);
  }

  function toggleMobileNav(e) {
    var nav = document.getElementById("freetools-v3--nav-full-id")
    nav.classList.toggle('nav-active');
    document.querySelector('.freetools-v3--mobile-nav-btn').classList.toggle('is-on');
  }

  function selectedVal(pulldown) {
    return pulldown.options[pulldown.options.selectedIndex].value;
  }

  function highlightForm(pulldown) {
    pulldown.style.boxShadow = "0 1px 6px rgba(255,0,0,.8)";
    pulldown.style.border = "1px solid red";
  }

  function cleanFormStyle(e) {
    var pulldown = e.target;
    pulldown.style.boxShadow = "none";
    pulldown.style.border = "1px solid #DDD";
  }

  function stateSelectorInit() {
    var linkPerState = {
      AL: "/alabama-dmv-change-of-address",
      AK: "/alaska-dmv-change-of-address",
      AZ: "/arizona-dmv-change-of-address",
      AR: "/arkansas-dmv-change-of-address",
      CA: "/california-dmv-change-of-address",
      CO: "/colorado-dmv-change-of-address",
      CT: "/connecticut-dmv-change-of-address",
      DE: "/delaware-dmv-change-of-address",
      FL: "/florida-dmv-change-of-address",
      GA: "/georgia-dmv-change-of-address",
      HI: "/hawaii-dmv-change-of-address",
      ID: "/idaho-dmv-change-of-address",
      IL: "/illinois-dmv-change-of-address",
      IN: "/indiana-dmv-change-of-address",
      IA: "/iowa-dmv-change-of-address",
      KS: "/kansas-dmv-change-of-address-1",
      KY: "/kentucky-dmv-change-of-address",
      LA: "/louisiana-dmv-change-of-address",
      ME: "/maine-dmv-change-of-address",
      MD: "/maryland-dmv-change-of-address",
      MA: "/massachusetts-dmv-change-of-address",
      MI: "/michigan-dmv-change-of-address",
      MN: "/minnesota-dmv-change-of-address",
      MS: "/mississippi-dmv-change-of-address",
      MO: "/missouri-dmv-change-of-address",
      MT: "/montana-dmv-change-of-address",
      NE: "/nebraska-dmv-change-of-address",
      NV: "/nevada-dmv-change-of-address",
      NH: "/new-hampshire-dmv-change-of-address",
      NJ: "/newjersey-dmv-change-of-address",
      NM: "/newmexico-dmv-change-of-address",
      NY: "/new-york-dmv-change-of-address",
      NC: "/north-carolina-dmv-change-of-address",
      ND: "/north-dakota-dmv-change-of-address",
      OH: "/ohio-dmv-change-of-address",
      OK: "/oklahoma-dmv-change-of-address",
      OR: "/oregon-dmv-change-of-address",
      PA: "/pennsylvania-dmv-change-of-address",
      RI: "/rhode-island-dmv-change-of-address",
      SC: "/south-carolina-dmv-change-of-address",
      SD: "/south-dakota-dmv-change-of-address",
      TN: "/tennessee-dmv-change-of-address",
      TX: "/texas-dmv-change-of-address",
      UT: "/utah-dmv-change-of-address",
      VT: "/vermont-dmv-change-of-address",
      VA: "/virginia-dmv-change-of-address",
      WA: "/washington-dmv-change-of-address",
      DC: "/washington-dc-dmv-change-of-address",
      WV: "/westvirginia-dmv-change-of-address",
      WI: "/wisconsin-dmv-change-of-address",
      WY: "/wyoming-dmv-change-of-address"
    };

    function checkState() {
      if (selectedVal(stateOptions) === "") {
        highlightForm(stateOptions);
        return;
      } else {
        window.open(linkPerState[selectedVal(stateOptions)], '_blank');
      }
    }

    var stateOptions = document.querySelector('#freetools-v3--state-select-field');
    var stateBtn = document.querySelector('#freetools-v3--state-select-btn');
    stateBtn.addEventListener('click', checkState);
    stateOptions.addEventListener('change', cleanFormStyle);

  }

  function initToastNotification() {
    function getLocalStorageValue() {
      var itemStr = localStorage.getItem('visitedTime');
      if (!itemStr) {
        return null;
      }

      var item = JSON.parse(itemStr);
      var now = new Date().getTime();

      if (now > item.expiry) {
        localStorage.removeItem('visitedTime');
        return null;
      }
      return item.value;
    }

    function setLocalStorageWithExpiry() {
      var now = new Date().getTime();
      var saveToken = {
        value: now,
        expiry: now + 259200000
      }
      localStorage.setItem('visitedTime', JSON.stringify(saveToken));
    }

    function showToast() {
      toastNotice.classList.add('active');
      toastNotice.querySelector('.notice-cta').addEventListener('click', hideToast);

    }

    function hideToast() {

      if (!lastVisitedTime) {
        setLocalStorageWithExpiry()
      }
      toastNotice.classList.remove('active');
      toastNotice.querySelector('.notice-cta').removeEventListener('click', hideToast);
    }

    var toastNotice = document.querySelector('.toast-notification');
    var lastVisitedTime = getLocalStorageValue();
    if (!lastVisitedTime) {
      showToast();
    } else {
      hideToast();
    }
  }

  var init = function () {
    var blogPreview = document.querySelector('.blog-preview-2018');
    var blogFlkty = new Flickity(blogPreview, {
      wrapAround: true

    })
    initToastNotification();
    freetoolsTabInit();
    stateSelectorInit();
    // internetSelectorInit();
    currentNav("I'm moving"); // Set current item in top nav
    initNav(); // Initialize mobile menu on smaller viewport
  }
  window.addEventListener('DOMContentLoaded', init);
</script>
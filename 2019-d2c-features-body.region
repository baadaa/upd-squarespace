<section class="d2c-2019--features">
  <section class="d2c-2019--features-hero">
    <section class="d2c-2019--features-hero-content container-2018">
      <section class="d2c-2019--features-hero-img">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/f_auto/v1552582628/website/2018-revamp-assets/thumbnails/updater-app-sliding.gif"
          alt="Updater App" />
      </section>
      <section class="d2c-2019--features-hero-text">
        <h1>Everything you need for your move.</h1>
        <h3>
          We break your move into bite-sized tasks and help you complete them,
          saving you time and money in the process.
        </h3>
        <svg>
          <use xlink:href="/assets/feature-list-svg-icon-defs.svg#down-arrow"></use>
        </svg>
      </section>
    </section>
  </section>
  <section class="d2c-2019--features-category container-2018 prepare">
    <section class="d2c-2019--features-highlight">
      <h2>Prepare for moving day</h2>
      <p>
        Whether you're hiring a full service moving company or renting a truck
        with your pals, we make it easy to compare your options and book
        everything you need.
      </p>
      <ul class="feature-list">
        <li>
          <span>
            <svg class="schedule-online">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#schedule-online"></use>
            </svg>
          </span>
          Reserve a mover online
        </li>
        <li>
          <span>
            <svg class="estimate">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#estimate"></use>
            </svg>
          </span>
          Request moving estimates
        </li>
        <li>
          <span>
            <svg class="truck">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#truck"></use>
            </svg>
          </span>
          Rent a truck
        </li>
        <li>
          <span>
            <svg class="hire-labor">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#hire-labor"></use>
            </svg>
          </span>
          Hire moving labor
        </li>
        <li>
          <span>
            <svg class="ship-car">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#ship-car"></use>
            </svg>
          </span>
          Ship your car
        </li>
        <li>
          <span>
            <svg class="trash">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#trash"></use>
            </svg>
          </span>
          Throw out your junk
        </li>
      </ul>
    </section>
    <section class="d2c-2019--features-image">
      <img
        src="https://res.cloudinary.com/updater-marketing/image/upload/f_auto/v1559943983/website/2019-d2c-direction/app-shots/moveiq_2x.png"
        alt="Find a mover" />
    </section>
  </section>
  <section class="d2c-2019--features-category container-2018 setup">
    <section class="d2c-2019--features-highlight">
      <h2>Set up your services</h2>
      <p>
        Why call around for hours when you can just use Updater? Hang up the
        phone and let us help you shop, compare, purchase, and install.
      </p>
      <ul class="feature-list">
        <li>
          <span>
            <svg class="tv-internet">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#tv-internet"></use>
            </svg>
          </span>
          Internet & TV
        </li>
        <li>
          <span>
            <svg class="insurance">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#insurance"></use>
            </svg>
          </span>
          Insurance
        </li>
        <li>
          <span>
            <svg class="electricity">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#electricity"></use>
            </svg>
          </span>
          Electricity
        </li>
        <li>
          <span>
            <svg class="water">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#water"></use>
            </svg>
          </span>
          Water
        </li>
        <li>
          <span>
            <svg class="gas">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#gas"></use>
            </svg>
          </span>
          Gas
        </li>
      </ul>
    </section>
    <section class="d2c-2019--features-image fliporder">
      <img
        src="https://res.cloudinary.com/updater-marketing/image/upload/f_auto/v1559943983/website/2019-d2c-direction/app-shots/internet-browse.png"
        alt="Set up TV & Internet" />
    </section>
  </section>
  <section class="d2c-2019--features-category container-2018 pack">
    <section class="d2c-2019--features-highlight">
      <h2>Pack it up</h2>
      <p>
        Need help figuring out how many boxes you'll need? How about a curated,
        interactive moving checklist? We've got you covered with hundreds of
        moving tips, tricks, and hacks.
      </p>
      <ul class="feature-list">
        <li>
          <span>
            <svg class="buy-boxes">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#buy-boxes"></use>
            </svg>
          </span>
          Buy boxes
        </li>
        <li>
          <span>
            <svg class="checklist">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#checklist"></use>
            </svg>
          </span>
          Moving checklists
        </li>
        <li>
          <span>
            <svg class="box-guide">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#box-guide"></use>
            </svg>
          </span>
          Box size guides
        </li>
        <li>
          <span>
            <svg class="checklist">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#checklist"></use>
            </svg>
          </span>
          Packing lists
        </li>
        <li>
          <span>
            <svg class="calculator">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#calculator"></use>
            </svg>
          </span>
          Moving calculator
        </li>
      </ul>
    </section>
    <section class="d2c-2019--features-image">
      <img
        src="https://res.cloudinary.com/updater-marketing/image/upload/f_auto/v1560185714/website/2019-d2c-direction/app-shots/box-packages_2x_2.png"
        alt="Pack it up" />
    </section>
  </section>
  <section class="d2c-2019--features-category container-2018 personalize">
    <section class="d2c-2019--features-highlight">
      <h2>Personalize your home</h2>
      <p>
        Make your new home <em>yours</em>. Get it set up the way you want, with as many bells and whistles as you want,
        all within Updater.
      </p>
      <ul class="feature-list">
        <li>
          <span>
            <svg class="home-improvement">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#home-improvement"></use>
            </svg>
          </span>
          Home improvement
        </li>
        <li>
          <span>
            <svg class="cleaning-services">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#cleaning-services"></use>
            </svg>
          </span>
          Cleaning services
        </li>
        <li>
          <span>
            <svg class="smart-home">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#smart-home"></use>
            </svg>
          </span>
          Smart home automation
        </li>
        <li>
          <span>
            <svg class="home-security">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#home-security"></use>
            </svg>
          </span>
          Home security
        </li>
      </ul>
    </section>
    <section class="d2c-2019--features-image fliporder">
      <img
        src="https://res.cloudinary.com/updater-marketing/image/upload/f_auto/v1578595635/website/2019-d2c-direction/app-shots/personalize-home_2x_2.png"
        alt="Update address" />
    </section>
  </section>
  <section class="d2c-2019--features-category container-2018 update">
    <section class="d2c-2019--features-highlight">
      <h2>Update your address</h2>
      <p>
        We make forwarding your mail and changing your address with the
        businesses you care about smooth and painless.
      </p>
      <ul class="feature-list">
        <li>
          <span>
            <svg class="mailforwarding">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#mailforwarding"></use>
            </svg>
          </span>
          Forward mail
        </li>
        <li>
          <span>
            <svg class="address-update">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#address-update"></use>
            </svg>
          </span>
          Update address everywhere
        </li>
        <li>
          <span>
            <svg class="driver-license">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#driver-license"></use>
            </svg>
          </span>
          Driver's license
        </li>
        <li>
          <span>
            <svg class="voter-registration">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#voter-registration"></use>
            </svg>
          </span>
          Voter registration
        </li>
      </ul>
    </section>
    <section class="d2c-2019--features-image">
      <img
        src="https://res.cloudinary.com/updater-marketing/image/upload/f_auto/v1559661347/website/2019-d2c-direction/app-shots/update-address.png"
        alt="Update address" />
    </section>
  </section>
  <section class="d2c-2019--features-category container-2018 save-money">
    <section class="d2c-2019--features-highlight">
      <h2>Save your money</h2>
      <p>
        We partner with your favorite national brands and local businesses to
        provide money-saving offers &mdash; available only on Updater.
      </p>
      <ul class="feature-list">
        <li>
          <span>
            <svg class="furniture">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#furniture"></use>
            </svg>
          </span>
          Furniture
        </li>
        <li>
          <span>
            <svg class="groceries">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#groceries"></use>
            </svg>
          </span>
          Groceries
        </li>
        <li>
          <span>
            <svg class="decor">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#decor"></use>
            </svg>
          </span>
          Decor
        </li>
        <li>
          <span>
            <svg class="appliance">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#appliances"></use>
            </svg>
          </span>
          Appliances
        </li>
        <li>
          <span>
            <svg class="cleaning-services">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#local-guide"></use>
            </svg>
          </span>
          Local businesses
        </li>
        <li>
          <span> </span>
          ...and much more
        </li>
      </ul>
    </section>
    <section class="d2c-2019--features-image fliporder">
      <img
        src="https://res.cloudinary.com/updater-marketing/image/upload/f_auto/v1578412971/website/2019-d2c-direction/app-shots/coupons-trimmed-white.png"
        alt="Save money" />
    </section>
  </section>
  <section class="d2c-2019--features-category container-2018 custom-needs">
    <section class="d2c-2019--features-highlight">
      <h2>Handle the rest</h2>
      <p>
        Worried about all the little things you might forget? We’ve got you
        covered.
      </p>
      <ul class="feature-list">
        <li>
          <span>
            <svg class="local-guide">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#local-guide"></use>
            </svg>
          </span>
          Local guides
        </li>
        <li>
          <span>
            <svg class="school-info">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#school-info"></use>
            </svg>
          </span>
          School information
        </li>
        <li>
          <span>
            <svg class="pet">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#pet"></use>
            </svg>
          </span>
          Pet requirements
        </li>
        <li>
          <span>
            <svg class="freight-elevator">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#freight-elevator"></use>
            </svg>
          </span>
          Freight elevator booking
        </li>
        <li>
          <span>
            <svg class="handbook">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#handbook"></use>
            </svg>
          </span>
          Community handbook
        </li>
        <li>
          <span>
            <svg class="events">
              <use xlink:href="/assets/feature-list-svg-icon-defs.svg#events"></use>
            </svg>
          </span>
          Neighborhood events
        </li>
      </ul>
    </section>
    <section class="d2c-2019--features-image">
      <img
        src="https://res.cloudinary.com/updater-marketing/image/upload/f_auto/v1560273209/website/2019-d2c-direction/app-shots/community-resources_r2_2x.png"
        alt="Info Center" />
    </section>
  </section>
</section>
<section class="get-started--to-claim">
  <h4>Want to use Updater?</h4>
  <h6>
    If you haven't received an invite, ask your property management company,
    real estate agent, loan officer, or moving company if they offer Updater.
    And in the meantime, check out our free tools to start conquering your to-do
    list.
  </h6>
  <a href="/free-moving-tools">View free tools</a>
</section>

<squarespace:script src="upd-ui-bits_20_07.min.js" combo="true" />

<script>
  var init = function () {
    currentNav("I'm moving"); // Set current item in top nav
    initNav(); // Initialize mobile menu on smaller viewport
    cta_top_and_bottom_nav('moving-support');
  };
  window.addEventListener('DOMContentLoaded', init);;
</script>
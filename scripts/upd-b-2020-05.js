//
// NOTE:
//
// - This file contains all the new UI methods specific to 2018 revamp.
// - The legacy code has contained lots of jQuery methods. Because replacing everything is a huge task, I kept most of them as-is.
// - In each template region files, where a template-specific methods are required, invocation of relevant methods are included in the bottom area.

//
// 1. TOP NAV: CURRENT ITEM

function currentNav(nav, option) {
  // NOTE: 'option' is an optional string "d2c" for May 2019 UI change. If not provided, it follows December 2018 markup.
  var navItems = document.querySelectorAll(
    '.b2b-2020-nav--desktop .nav-items > li'
  );
  navItems.forEach(function(li) {
    if (
      li.innerText
        .toLowerCase()
        .trim()
        .startsWith(nav.toLowerCase())
    ) {
      li.classList.add('active');
    }
  });
}

//
// 2. MOBILE MENU

function initNav(option) {
  // NOTE: 'option' is an optional string "d2c" for May 2019 UI change. If not provided, it follows December 2018 markup.
  // TODO: Once D2C UI change launches, remove the optional statements here.
  /* eslint-disable */
  (function(l){var i,s={touchend:function(){}};for(i in s)l.addEventListener(i,s)})(document); // sticky hover fix in iOS

  var resizeTimeout;
  var toggleMenu = document.getElementById('mobile-toggle');
  var loginBtn = document.querySelector('.top-ctas--login');
  var mobileSubItemTrigger = document.querySelectorAll('.b2b-mobile-nested-btn');
  var mobileTurnOff = function() {
    toggleMenu.classList.remove('is-on');
    document.body.style.overflow = '';
  };
  var mobileTurnOn = function() {
    toggleMenu.classList.add('is-on');
    document.body.style.overflow = 'hidden!important';
  };
  var mobileMenu = function() {
    toggleMenu.classList.contains('is-on') ? mobileTurnOff() : mobileTurnOn();
  };
  var collapseAllMobileToggle = function() {
    document.querySelectorAll('.b2b-mobile-nested').forEach(function(item) {
      item.style.flex ='3';
      item.style.background='white';
      var child = item.querySelector('.b2b-mobile-nested-options');
      child.classList.remove('is-on');
    });
  }
  var mobileSubItemToggle = function(e) {
    var clickedItem = e.target;
    var mobileNestedOptions = clickedItem.parentElement.querySelector(
      '.b2b-mobile-nested-options'
      );
      var mobileNestedMenu = clickedItem.parentElement;
      function expandOptions() {
        collapseAllMobileToggle();
      // console.log(mobileNestedOptions);
      // console.log('expanding')
      mobileNestedOptions.classList.add('is-on');
      mobileNestedMenu.style.flex = '5';
      mobileNestedMenu.style.background='#e1f4f7';
    }
    function collapseOptions() {
      // console.log(mobileNestedOptions);
      // console.log('collapsing')
      mobileNestedOptions.classList.remove('is-on');
      mobileNestedMenu.style.flex = '3';
      mobileNestedMenu.style.background='white';
    }
    return mobileNestedOptions.classList.contains('is-on')
      ? collapseOptions()
      : expandOptions();
  };
  function loginOptionsOff() {
    loginBtn.classList.remove('is-on');
    document.querySelector('.top-ctas--login-sub').classList.remove('is-on');
  }
  function loginOptionsToggle() {
    loginBtn.classList.toggle('is-on');
    document.querySelector('.top-ctas--login-sub').classList.toggle('is-on');
  }
  function actualResizeHandler() {
    if (window.innerWidth < 900) {
      // Mobile menu break-point
      return;
    }
    loginOptionsOff();
    if (toggleMenu.classList.contains('is-on')) {
      mobileTurnOff();
    }
  }
  function resizeThrottler() {
    // ignore resize events as long as an actualResizeHandler execution is in the queue
    if (!resizeTimeout) {
      resizeTimeout = setTimeout(function() {
        resizeTimeout = null;
        actualResizeHandler();
        // The actualResizeHandler will execute at a rate of 15fps
      }, 66);
    }
  }

  document.getElementById('mobile-menu').addEventListener('click', mobileMenu);
  loginBtn.addEventListener('click', loginOptionsToggle);

  // if (option === 'd2c' || option ==='b2b') {
    mobileSubItemTrigger.forEach(function(item){item.addEventListener('click', mobileSubItemToggle)});
  // }
  window.addEventListener('resize', resizeThrottler, false);
}

//
// 3. BLOG LIST
function blogListHandler() {
  var notFirstPage = window.location.href.indexOf('offset') !== -1;
  var filteredByAuthor = window.location.href.indexOf('author') !== -1;
  var filteredByTag = window.location.href.indexOf('tag') !== -1;
  var movingTipsBlog = window.location.href.indexOf('moving-tips') !== -1;
  var filterTitle;
  var blogListItems;
  var firstTwo;
  var firstOne;
  var mostRecentPosts;
  // Hide big two latest items contextually
  if (notFirstPage || filteredByAuthor || filteredByTag) {
    mostRecentPosts = document.querySelector('.blog-list--most-recent');
    mostRecentPosts.style.display = 'none';
    mostRecentPosts.remove();
    if (notFirstPage && !filteredByAuthor && !filteredByTag) {
      filterTitle = document.querySelector('.blog-list--filtered');
      filterTitle.style.display = 'none';
    }
  } else {
    blogListItems = document.querySelectorAll('.blog-list--item');
    firstTwo = [blogListItems[0], blogListItems[1]];
    // eslint-disable-next-line prefer-destructuring
    firstOne = blogListItems[0];
      firstTwo.forEach(function(item) {
        item.style.display = 'none';
        item.remove();
      });
    filterTitle = document.querySelector('.blog-list--filtered');
    filterTitle.style.display = 'none';
  }
}

// Utility Function for CSS property checking
function cssPropertyValueSupported(prop, value) {
  // NOTE: check if the current browser supports a CSS property
  // e.g. cssPropertyValueSupportyed('position', 'sticky') will return a Boolean
  var d = document.createElement('div');
  d.style[prop] = value;
  return d.style[prop] === value;
}

//
// 4. BLOG STICKY SIDEBAR

function blogSticky() {
  var cssStickySupported = cssPropertyValueSupported('position', 'sticky');

  var stickyTop = $('.stick').offset().top;
  var stopPt = $('.stick-stop').offset().top + $('.stick-stop').height();
  var catcher = $('.stick-catch');

  $(window).scroll(function() {
    var scrWidth = $(window).innerWidth();
    var scrollTop = $(window).scrollTop() + 105;

    var stickIt = function() {
      // console.log('sticking');
      if (cssStickySupported) {
        $('.stick')
          .css('position', 'sticky')
          .css('transform', 'translateY(120px)');
      } else {
        // console.log('not css sticky ')
        $('.stick')
          .css('position', 'absolute')
          .css('top', scrollTop-160+'px');
      }
    };

    var unStickIt = function() {
      // console.log('unSticking')
      if (cssStickySupported) {
        // $('.stick').css('position', 'sticky').css('transform', 'translateY(60px)');

        $('.stick')
          .css('position', 'relative')
          .css('top', 0)
          .css('transform', 'translateY(0)');
      } else {
        $('.stick')
          .css('position', 'relative')
          .css('top', 0);
      }
    };

    var stickBottom = function() {
      // console.log('stickBottom');
      if (cssStickySupported) {
        // console.log('bottom');
        $('.stick').css('margin-bottom', '120px');
        // $('.stick').css('position', 'absolute').css('top', stopPt - $('.stick').height()-120);
      } else {
        $('.stick')
          .css('position', 'absolute')
          .css('top', stopPt - $('.stick').height() - 120);
      }
    };

    var defaultPos = function() {
      if (catcher.offset().top + catcher.height() > $('.stick').offset().top) {
        // $('.stick').css('position', 'absolute').css('bottom', catcher.offset().top + catcher.height());
      }
    };
    if (scrWidth <= 992) {
      // console.log('small');
      unStickIt();
      return;
    }

    scrollTop > stickyTop ? stickIt() : unStickIt();

    // console.log("$('.stick').offset().top + $('.stick').height()", $('.stick').offset().top + $('.stick').height(), 'stopPt', stopPt);

    $('.stick').offset().top + $('.stick').height() > stopPt
      ? stickBottom()
      : defaultPos();

    // if ($('.stick').offset().top + $('.stick').height() > stopPt) {
    //   // Reaching the stopping point on bottom
    //   $('.stick').css('position', 'absolute').css('top', stopPt - $('.stick').height()-120);
    //   } else if (catcher.offset().top + catcher.height() > $('.stick').offset().top){
    //   // Reaching the stopping point on top
    //   }
  });
}

//
// 5. Sticky Subnav: Relo products and Resources
function jumpToStick(e) {
  // NOTE: Add native CSS scroll behavior

  e.preventDefault();
  // console.log(e, e.target, e.target.hash);
  $('html, body').animate(
    {
      scrollTop: $(e.target.hash).offset().top - 120,
    },
    600
  ); // scroll to position in 0.6 sec
}

function stickySubnav() {
  // TODO: Make DOM selector updates per D2C update

  var stickyNavTop = $('.revamp-2018--sticky-wrapper').offset().top;

  function stickyPos() {
    var isInvestorPage = window.location.href.indexOf('investor') !== -1;
    var isWiderThan900 = window.innerWidth > 900;
    var navbarAdjustment = 77;
    // var stickyAdjustment = isWiderThan900 ? '70px' : '39px';
    var scrollTop = $(window).scrollTop() + navbarAdjustment;
    if (scrollTop > stickyNavTop) {
      $('.revamp-2018--sticky-wrapper')
        .css('position', 'fixed')
        .css('top', (navbarAdjustment - 39) + 'px')
        .css('width', '100%')
        .css('z-index', '11');
      document.querySelector('.first-subsection').style.marginTop = '90px';
      var highLightedNavItem = document.querySelector('.nav-items > li.active');
      !isInvestorPage && highLightedNavItem
        ? (highLightedNavItem.style.borderBottom = '5px solid #334D5C')
        : console.log('');
    } else {
      $('.revamp-2018--sticky-wrapper').removeAttr('style');
      document.querySelector('.first-subsection').style.marginTop = 0;
      !isInvestorPage && highLightedNavItem
        ? (highLightedNavItem.style.borderBottom = '5px solid #F5333F')
        : console.log('');
    }
  }

  document.querySelectorAll('.revamp-2018--sticky li').forEach(function(li) {
    li.addEventListener('click', jumpToStick);
  });
  $(window).scroll(function() {
    stickyPos();
  });
}

//
// 6. Toggle Features: Relo Solutions

function toggleReFeatures() {
  var toggleItems = document.querySelectorAll(
    '.business-re--features-list li, .mover-app--features-list li'
  );
  var toggleContents = document.querySelectorAll(
    '.business-re--features-content, .mover-app--features-content'
  );
  function toggleSections(e) {
    // console.log(e.target.id, e.target);
    if (e.target.classList.contains('active')) {
      return;
    }
    toggleItems.forEach(function(item) {
      item.classList.remove('active');
      if (item.classList.contains(e.target.id)) {
        // console.log(item);
        item.classList.add('active');
      }
    });
    document.getElementById(e.target.id).classList.add('active');
    toggleContents.forEach(function(item) {
      item.classList.remove('active');

      if (item.classList.contains(e.target.id)) {
        item.classList.add('active');
      }
    });
  }
  toggleItems.forEach(function(item) {
    item.addEventListener('click', toggleSections);
  });
}

//
// 7. Business Products circle elements

function handleBizCircles() {
  var moverCircle = {
    dom: document.querySelector('#movers'),
    origin: { cx: 249, cy: 331, r: 93 },
    biz: { cx: 240, cy: 330, r: 60 },
    re: { cx: 270, cy: 300, r: 60 },
    mover: { cx: 310, cy: 290, r: 110 },
  };
  var bizCircle = {
    dom: document.querySelector('#businesses'),
    origin: { cx: 413, cy: 93, r: 93 },
    biz: { cx: 390, cy: 112, r: 110 },
    re: { cx: 370, cy: 150, r: 60 },
    mover: { cx: 440, cy: 70, r: 60 },
  };
  var reCircle = {
    dom: document.querySelector('#realestate'),
    origin: { cx: 93, cy: 93, r: 93 },
    biz: { cx: 125, cy: 160, r: 60 },
    re: { cx: 112, cy: 112, r: 110 },
    mover: { cx: 125, cy: 210, r: 60 },
  };
  var moverTxt = {
    dom: document.querySelector('#mtxt'),
    origin: { transform: 'matrix(1 0 0 1 203 339)' },
    biz: { transform: 'matrix(.75 0 0 .75 205 339)' },
    re: { transform: 'matrix(.75 0 0 .75 237 305)' },
    mover: { transform: 'matrix(1.15 0 0 1.15 260 300)' },
  };
  var bizText = {
    dom: document.querySelector('#btxt'),
    origin: { transform: 'matrix(1 0 0 1 339.812 101.048)' },
    biz: { transform: 'matrix(1.15 0 0 1.15 310.812 121.048)' },
    re: { transform: 'matrix(.75 0 0 .75 316.812 155.048)' },
    mover: { transform: 'matrix(.75 0 0 .75 385.812 75.048)' },
  };
  var reText1 = {
    dom: document.querySelector('#rtxt1'),
    origin: { transform: 'matrix(1 0 0 1 19.764 93.048)' },
    biz: { transform: 'matrix(.75 0 0 .75 73.764 158.048)' },
    re: { transform: 'matrix(1.15 0 0 1.15 30.764 110.048)' },
    mover: { transform: 'matrix(.75 0 0 .75 73.764 210.048)' },
  };
  var reText2 = {
    dom: document.querySelector('#rtxt2'),
    origin: { transform: 'matrix(1 0 0 1 37.62 117.048)' },
    biz: { transform: 'matrix(.75 0 0 .75 84.62 176.048)' },
    re: { transform: 'matrix(1.15 0 0 1.15 50.62 138.048)' },
    mover: { transform: 'matrix(.75 0 0 .75 84.62 226.048)' },
  };
  var line1 = {
    dom: document.querySelector('#line1'),
    origin: { d: 'M249,331L413,93' },
    biz: { d: 'M240.5,320L455,63' },
    re: { d: 'M280.5,282L410,110' },
    mover: { d: 'M310,290L440,70' },
  };
  var line2 = {
    dom: document.querySelector('#line2'),
    origin: { d: 'M100,93l147.5,237' },
    biz: { d: 'M100,130l137.5,200' },
    re: { d: 'M100,130l210.5,200' },
    mover: { d: 'M114,210L310,290' },
  };
  var line3 = {
    dom: document.querySelector('#line3'),
    origin: { d: 'M395,93L100,93' },
    biz: { d: 'M395,113L120,160' },
    re: { d: 'M395,143L137.5,100' },
    mover: { d: 'M440,70L125,210' },
  };
  var circleElem = {
    isClicked: false,
    active: '',
  };
  function redrawCircles(mode) {
    [
      moverCircle,
      reCircle,
      bizCircle,
      bizText,
      moverTxt,
      reText1,
      reText2,
      line1,
      line2,
      line3,
    ].forEach(function(item) {
      var target = item[mode];
      var attrs = Object.keys(target);
      attrs.forEach(function(attr, i) {
        item.dom.setAttribute(attr, target[attr]);
      });
    });
  }
  function closeBPModal(e) {
    var modal = ".business-products--content-" + e.target.id;
    // console.log(modal);
    document.querySelector(modal).classList.remove('isActive');
    document
      .querySelector('.business-products--overview-modal-bg')
      .classList.remove('isFocused');
    redrawCircles('origin');
    document.querySelectorAll('.bp-lines').forEach(function(line) {
      line.classList.remove('isFocused');
    });
    circleElem.isClicked = false;
  }

  function closeBPModalAll() {
    [
      '.business-products--content-realestate',
      '.business-products--content-businesses',
      '.business-products--content-movers',
    ].forEach(function(str) {
      document.querySelector(str).classList.remove('isActive');
    });
  }
  function applyBackground(audience) {
    var bg = document.querySelector('.business-products--overview-modal-bg');
    var bgSize = {
      movers: 'cover, 1px, 1px',
      businesses: '1px, cover, 1px',
      realestate: '1px, 1px, cover',
    };
    var bgColor = {
      movers: 'rgba(56, 76, 90,.8)',
      businesses: 'rgba(56, 76, 90,.4)',
      realestate: 'rgba(56, 76, 90,.8)',
    };
    bg.style.backgroundSize = bgSize[audience];
    bg.style.backgroundColor = bgColor[audience];
  }
  function clicked(e) {
    var newMode = '';
    if (circleElem.isClicked) {
      if (circleElem.active === e.target.id) {
        return;
      }
      if (e.target.id === 'movers') {
        newMode = 'mover';
      }
      if (e.target.id === 'businesses') {
        newMode = 'biz';
      }
      if (e.target.id === 'realestate') {
        newMode = 're';
      }
      redrawCircles(newMode);
      closeBPModalAll();
    }
    circleElem.isClicked = true;
    applyBackground(e.target.id);
    document
      .querySelector('.business-products--overview-modal-bg')
      .classList.add('isFocused');
    circleElem.active = e.target.id;
    document.querySelectorAll('.bp-lines').forEach(function(line) {
      line.classList.add('isFocused');
    });
    document
      .querySelector('.business-products--content-' + e.target.id)
      .classList.add('isActive');
  }

  function mouseIn(e) {
    if (circleElem.isClicked) {
      return;
    }

    switch (e.target.id) {
      case 'businesses':
        redrawCircles('biz');
        break;
      case 'realestate':
        redrawCircles('re');
        break;
      case 'movers':
        redrawCircles('mover');
        break;
      default:
        console.log('not happening');
    }
  }
  function mouseOut(e) {
    if (circleElem.isClicked) {
      return;
    }

    redrawCircles('origin');
  }

  document
    .querySelectorAll('.business-products--modal-close')
    .forEach(function(btn) {
      btn.addEventListener('click', closeBPModal);
    });

  [moverCircle.dom, bizCircle.dom, reCircle.dom].forEach(function(circle) {
    circle.addEventListener('click', clicked);
    circle.addEventListener('mouseenter', mouseIn);
    circle.addEventListener('mouseout', mouseOut);
  });
}

//
// 9. Flip card on home page

function cardFlip() {
  var flipToggle = function(e) {
    var targetCard = e.target.classList[1];
    var targetDiv = document.querySelector('div.home-2018--card.'+targetCard);
    e.preventDefault();
    targetDiv ? targetDiv.classList.toggle('hover') : console.log('no flip');
  };
  var cardBtns = document.querySelectorAll('a.card-cta, span.card-close');
  cardBtns.forEach(function(btn) {
    if (btn.classList.contains('no-flip')) {
      console.log('noflip');
    } else {
      btn.addEventListener('click', flipToggle);
    }
  });
}

//
// 10. Kandela forms
// TODO: remove this function element

function kandelaFormSubmitInit() {
  function kandelaFormCheck(e) {
    var k_name = document.querySelector('input#callee_name').value.trim();
    var k_number = document.querySelector('input#callee_number').value.trim();
    e.preventDefault();
    if (k_name === '') {
      document.querySelector('input#callee_name').style.border =
        '2px solid red';
      document.querySelector('input#callee_name').placeholder = 'Enter name';
    } else {
      document.querySelector('input#callee_name').style.border = 'none';
    }
    if (k_number === '') {
      document.querySelector('input#callee_number').style.border =
        '2px solid red';
      document.querySelector('input#callee_number').placeholder =
        'Enter phone number';
    } else {
      document.querySelector('input#callee_number').style.border = 'none';
    }

    if (k_name !== '' && k_number !== '') {
      document.querySelector('.kandela-success-msg').classList.add('active');
    }
  }
  document
    .querySelector('#kandela-lp--we-call-you--form-submit-btn')
    .addEventListener('click', kandelaFormCheck);
}

//
// 11. Kandela in your area
// TODO: Remove this function

function kandelaFindUsers() {
  var k_no = Math.floor(Math.random() * (100000 - 50000) + 50000).toString();
  
  var thousandPos = k_no.length - 3;
  
  var commaStr = k_no.slice(0, thousandPos) + ',' + k_no.slice(thousandPos);
  // var ip = ipLookUp();
  document.querySelector('.kandela-lp--in-your-area').textContent = commaStr;
}


// Utility Function for AddThis widget's presence
function addThisFooterHidden() {
  var addThisBottomDiv = document.querySelector(
    '.at-share-dock-outer.addthis-smartlayers.addthis-smartlayers-mobile'
  );
  if (!addThisBottomDiv) {
    return;
  }
  if (
    (addThisBottomDiv &&
      (addThisBottomDiv.classList.contains('at4-hide') ||
        addThisBottomDiv.classList.contains('at4-visually-hidden'))) ||
    document.getElementById('at-share-dock').classList.contains('at4-hide')
  ) {
    // console.log('addthis hidden');
    return false;
  }
  // console.log('addthis false');
  return true;
}


// Sticky CTAs
// TODO: Clean up per D2C updates

function cta_top_and_bottom_nav(cta_type, option) {
  var navbar = document.querySelector('.b2b-2020-header');
  var firstSection = document.querySelector('section:first-of-type');
  var navWrapper = document.querySelector('.b2b-2020-nav--wrapper');

  var originalNavbarHeight = navbar.offsetHeight;
  var ctaHeight = 50;
  var cta_on_top = document.querySelector('div.cta_above_top_nav');
  var cta_on_bottom = document.querySelector('div.cta_on_page_bottom');
  var mobileMenuOverlay = document.querySelector('.b2b-2020--mobile-menu');
  function hideCallToOrder() {
    document
      .querySelectorAll('.cta_on_page_bottom > .cta-call-to-order')
      .forEach(function(item) {
        item.style.display = 'none';
      });
  }
  function hideMovingSupport() {
    document
      .querySelectorAll('.cta_on_page_bottom > .cta-moving-support')
      .forEach(function(item) {
        item.style.display = 'none';
      });
  }

  cta_type === 'moving-support' ? hideCallToOrder() : hideMovingSupport();

  if (option !== 'no-top') {
    cta_on_top.style.display = 'block';
  }
  cta_on_bottom.style.display = 'block';

  navbar.insertBefore(cta_on_top, navWrapper);

  function hideCTAonBottom() {
    cta_on_bottom.style.transform = 'translateY(50px)';
  }
  function showCTAonBottom() {
    var heightAdjustment = 0;
    if (addThisFooterHidden()) {
      heightAdjustment = 48; // AddThis div height
    } else {
      heightAdjustment = 0;
    }
    cta_on_bottom.style.transform = "translateY(-"+ heightAdjustment + "px)";
  }
  function showCTAonTop() {
    cta_on_top.style.transform = "translateY(0)"
    navbar.style.height = originalNavbarHeight + ctaHeight + "px";
    navbar.style.paddingTop = ctaHeight + "px";
    firstSection.style.transform = "translateY("+ ctaHeight  +"px)";
    mobileMenuOverlay.style.top = originalNavbarHeight + ctaHeight + "px";
    hideCTAonBottom();
  }
  function hideCTAonTop() {
    cta_on_top.style.transform = "translateY(-" + ctaHeight+ "px)";
    navbar.style.height = originalNavbarHeight + "px";
    navbar.style.paddingTop = "0";
    firstSection.style.transform = "translateY(0)";
    mobileMenuOverlay.style.top = originalNavbarHeight + "px";
    showCTAonBottom();
  }
  function checkScroll(e) {
    if (window.scrollY >= 500) {
      hideCTAonTop();
    } else {
      option !== 'no-top' ? showCTAonTop() : hideCTAonTop();
    }
  }
  function initCTAonTop() {
    navbar.style.transition = 'all .2s';
    firstSection.style.transition = 'all .2s';
    cta_on_top.style.height = ctaHeight + "px";
    cta_on_top.style.lineHeight = ctaHeight + "px";
    option !== 'no-top' ? showCTAonTop() : hideCTAonTop();
  }
  initCTAonTop();

  window.addEventListener('scroll', checkScroll);
}

function iiForm_addthisAdjust() {
  var heightAdjustment = 0;
  var iiFormContainer = document.querySelector('.ii--lead-capture');
  console.log(iiFormContainer);
  function checkScrollII() {
    if (addThisFooterHidden()) {
      heightAdjustment = 48; // AddThis div height
      console.log('shown');
    } else {
      heightAdjustment = 0;
      console.log('hidden');
    }
    iiFormContainer.style.transform = "translateY(-"+ heightAdjustment + "px)";
  }
  window.addEventListener('scroll', checkScrollII);
}

function ii_lead_capture_init() {
  var capture_cta = document.querySelector('.ii--lead-capture-cta');
  var formArea = document.querySelector('.ii--lead-capture-form');
  var formClose = document.querySelector('.ii--lead-capture-form-close');
  function toggleOnOff() {
    formArea.classList.toggle('on');
  };

  capture_cta.addEventListener('click', toggleOnOff);
  formClose.addEventListener('click', toggleOnOff);
  iiForm_addthisAdjust();
}

function mktoFormChain(config) {

	/* util */
	var arrayFrom = Function.prototype.call.bind(Array.prototype.slice);

	/* const */
	var MKTOFORM_ID_ATTRNAME = "data-formId";
	
	/* fix inter-form label bug! */
	MktoForms2.whenRendered(function(form) {
		var formEl = form.getFormElem()[0],
			rando = "_" + new Date().getTime() + Math.random();

		arrayFrom(formEl.querySelectorAll("label[for]")).forEach(function(labelEl) {
			var forEl = formEl.querySelector('[id="' + labelEl.htmlFor + '"]');
			if (forEl) {
				labelEl.htmlFor = forEl.id = forEl.id + rando;
			}
		});
	});

	/* chain, ensuring only one #mktoForm_nnn exists at a time */
	arrayFrom(config.formIds).forEach(function(formId) {
		var loadForm = MktoForms2.loadForm.bind(MktoForms2,config.podId,config.munchkinId,formId),
			formEls = arrayFrom(document.querySelectorAll("[" + MKTOFORM_ID_ATTRNAME + '="' + formId + '"]'));

		(function loadFormCb(formEls) {
			var formEl = formEls.shift();
			formEl.id = "mktoForm_" + formId;

			loadForm(function(form) {
				formEl.id = "";
				if (formEls.length) {
					loadFormCb(formEls);
				}
			});
		})(formEls);
	});
}

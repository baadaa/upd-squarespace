/*
  Helpful Links
  =============

  https://yuilibrary.com/yui/docs/
  http://www.jsrosettastone.com/
  http://developers.squarespace.com/custom-javascript/
*/


// Create a YUI sandbox on your page.
YUI().use('node', 'event', function (Y) {
  Y.on('domready', function () {
    // The Node and Event modules are loaded and ready to use.
    // Your code goes here!
  });
});

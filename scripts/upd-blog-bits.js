//
// Moving Tips can contain 'sponsored links' in an image gallery format
// in which case, the post should contain (via Wysiwyg editor)
// a code block that defines the sponsored items.
//
// See /moving-tips/first-apartment-checklist post for reference.
//

function sponsorLinksInit() {
  var divs = document.querySelectorAll('.display-links');
  function processTags(item) {
    return (
      '<div class="display-item">' +
      '<a href="' +
      item.link +
      '" target="_blank" rel="noopener noreferrer"><img class="display-product" src="' +
      item.image +
      '" data-lazysrc="' +
      item.image +
      '" alt="' +
      item.text +
      '" /></a>' +
      '<a href="' +
      item.link +
      '" target="_blank" rel="noopener noreferrer"><span class="display-price">' +
      item.text +
      '</span></a></div>'
    );
  }

  if (divs.length === 0) return;

  divs.forEach(function(div) {
    var id = div.dataset.id;
    var items = sponsorLinks[div.dataset.id];
    // NOTE: `sponsorLinks` needs to be defined in the blog post itself
    var tags = [];
    items.forEach(function(item) {
      var tag = processTags(item);
      tags.push(tag);
    });
    div.innerHTML = tags.join('');
    div.querySelectorAll('.display-item').forEach(function(innerDiv) {
      var unitPercentage = (1 / items.length) * 100;
      var calcVal = 'calc(' + unitPercentage + '% - 10px)';
      innerDiv.style.flexBasis = calcVal;
    });
  });
}

// Blog reading time measure

function setReadingTime(option) {
  function toArray(list) {
    return Array.prototype.slice.call(list);
  }

  function totalNumberOfWords(array) {
    return array
      .map(function(elem) {
        return elem.textContent.split(' ').length;
      })
      .reduce(function(acc, cur) {
        return acc + cur;
      }, 0);
  }
  function blogWordCount() {
    var mainSection = document.querySelector('.blog-article--main-content');
    var bodySection = mainSection.querySelector(
      'div[data-layout-label="Post Body"'
    );
    var headings = toArray(bodySection.querySelectorAll('h1, h2, h3'));
    var paragraphs = toArray(bodySection.querySelectorAll('p'));
    var listItems = toArray(bodySection.querySelectorAll('li'));

    var headingWords = totalNumberOfWords(headings);
    var paragraphWords = totalNumberOfWords(paragraphs);
    var listItemWords = totalNumberOfWords(listItems);
    return headingWords + paragraphWords + listItemWords;
  }
  var totalWords = blogWordCount();
  var wordPerMinute = 265;
  var mins = Math.round(totalWords / wordPerMinute);
  var readingTime = mins;
  document.querySelector('.reading-time').innerText = readingTime;
  if (option === false) {
    document.querySelector('.blog-article--reading-time').remove();
    document
      .querySelector('.blog-article--post-meta[data-secondary="true"]')
      .removeAttribute('data-secondary');
  }
}

// Show FTC Affiliate Link disclaimer

function displayFTCdisclaimer() {
  var currentPath = window.location.pathname;
  var httpRequest = new XMLHttpRequest();

  function showDisclaimer() {
    var disclaimerText =
      'This post contains references to products from one or more of our partners and Updater may receive compensation when you purchase those products.';
    var el = document.createElement('div');
    el.style.padding = '15px 30px';
    el.style.fontSize = '11px';
    el.style.fontFamily = 'Merriweather, Georgia, serif';
    el.style.fontStyle = 'italic';
    el.style.marginBottom = '20px';
    el.style.lineHeight = '1.7';
    el.style.background = '#f2f3f4';
    el.style.color = '#666';

    el.innerText = disclaimerText;
    document
      .querySelector('.wo_cloudinary__blog-hero-wrapper')
      .insertAdjacentElement('afterend', el);
  }

  function hidePromptTag() {
    var tagList = document.querySelectorAll('.blog-article--tags li');
    tagList.forEach(function(item) {
      if (item.textContent.trim() === 'ftc') {
        item.remove();
      }
    });
  }

  function handleResponse() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      if (httpRequest.status === 200) {
        var response = JSON.parse(httpRequest.responseText);
        var tags = response.item.tags;
        if (tags.indexOf('ftc') !== -1) {
          showDisclaimer();
          hidePromptTag();
        }
      }
    }
  }

  function makeRequest() {
    httpRequest.onreadystatechange = handleResponse;

    httpRequest.open('GET', currentPath + '?format=json', true);
    httpRequest.send();
  }

  window.addEventListener('load', makeRequest);
}

// visual scroll indicator

function initScrollIndicator() {
  var scrollIndicator = '<div class="scroll-indicator"></div>';
  var elem = document.createElement('div');
  elem.classList.add('scroll-indicator--wrapper');
  elem.innerHTML = scrollIndicator;
  document.body.appendChild(elem);

  window.addEventListener('scroll', function() {
    var maxHeight = document.body.scrollHeight - window.innerHeight;
    var percentage = (window.scrollY / maxHeight) * 100;
    elem.querySelector('.scroll-indicator').style.width = percentage + '%';
  });
}

// Moving Tips in-text banner elements

function intextBannerInit() {
  var freeToolsBannerContent =
    "<div class='cicada'><h5>Free Moving Resources</h5><h6 style='max-width: 500px; margin-left: auto; margin-right: auto;line-height: 1.4;'>Everything &mdash; we mean <em>everything</em> &mdash; you need to conquer your move like a pro.</h6><a id='free-tools-page_in-text-banner' href='/free-moving-tools'>Check 'em out</a></div>";
  var internetBannerContent =
    "<a id='tv-internet-in-text-banner' href='/free-moving-tools' target='_blank'><img src='https://res.cloudinary.com/updater-marketing/image/upload/v1553707381/Blog%20In-text%20Banner%20Images/Current%20Banner/in-text-blog-banner_free-moving-tools.png'></a>";
  var claimUpdaterBannerContent =
    "<div class='cicada'><h5>Ready to get sh*t done?</h5><h6 style='max-width: 600px; margin-left: auto; margin-right: auto;line-height: 1.4;'>Make your move easier with <em>America's #1 moving app.</em></h6><a id='free-tools-page_in-text-banner' href='/claim'>Get started</a></div>";
  var intextBannerFreeToolsContent =
    "<div class='cicada'><h5>Ready to get sh*t done?</h5><h6 style='max-width: 600px; margin-left: auto; margin-right: auto;line-height: 1.4;'>Make your move easier with our free moving tools.</h6><a id='free-tools-page_in-text-banner' href='/free-moving-tools'>Get started</a></div>";

  var penskeBannerContent =
    "<div class='penske_banner'><img src='https://res.cloudinary.com/updater-marketing/image/upload/v1564172016/website/banners/penske_updater_banner_v3-optimized.svg' alt='Save on Penske rental Trucks'><a href='https://www.pensketruckrental.com/rental_entry.html?CID=80990120' rel='noreferrer noopener' target='_blank'><img class='moving-tips--cicada-footer-banner' id='penske-discount_moving-tips' src='https://res.cloudinary.com/updater-marketing/image/upload/v1537392285/website/2018-revamp-assets/photos/blank.gif' alt='Free Moving Tools'></a></div>";
  //

  var generalBanner = document.querySelector(
    '.moving-tips--in-text-banner-area'
  );

  var penskeBanner = document.querySelector('.moving-tips--penske-banner');

  var seeAlsoArea = document.querySelector('.moving-tips--see-also');

  if (penskeBanner) {
    penskeBanner.innerHTML = penskeBannerContent;
  }

  if (seeAlsoArea) {
    var seeAlsoContent =
      "<span>SEE ALSO: </span><a href='" +
      seeAlsoData.link +
      "'>" +
      seeAlsoData.text +
      '</a>';
    seeAlsoArea.innerHTML = seeAlsoContent;
  }
  if (generalBanner) {
    generalBanner.innerHTML = intextBannerFreeToolsContent;
  }
}

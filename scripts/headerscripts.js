$(document).ready(function() {

  function init() {
    FastClick.attach(document.body);
    headerScroll();
    backToTopScroll();
    bindEventsHere();
  }

  function headerScroll() {
    window.addEventListener('scroll', function (e) {
      var distanceY = window.pageYOffset || document.documentElement.scrollTop,
        shrinkOn = 100,
        nav = document.querySelector("nav");
      if (distanceY > shrinkOn) {
        classie.add(nav, "scrolled");
      } else {
        if (classie.has(nav, "scrolled")) {
          classie.remove(nav, "scrolled");
        }
      }
    });      
  }

  function backToTopScroll() {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 150) {
        $('#back-to-top').addClass('showme');
      } else {
        $('#back-to-top').removeClass('showme');
      }
    });  
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
      $('#back-to-top').tooltip('hide');
      $('body,html').animate({
        scrollTop: 0
      }, 800);
      return false;
    });
  }

  function bindEventsHere() {
    $('a.top-nav-trigger').on('click', function(e) {
      var targetType = $(e.target).attr('aria-controls');
      if ($('#products-panel > div.main-section').hasClass('in') && targetType !== 'updater-products'){
        $('#products-panel .collapse').removeClass('in');
      }
      if ($('#login-panel > div.main-section').hasClass('in') && targetType !== 'login-type') {
        $('#login-panel .collapse').removeClass('in');
      }
    });

    $('.login-menu').on('click', function(){
      if ($('#updater-products').hasClass('in')) {
        $('#products-panel .collapse').collapse('hide');
      }      
    });
    $(document).on('click', function () {
      if ($('#updater-products').hasClass('in')) {
        $('#products-panel .collapse').collapse('hide');
      }
      if ($('#login-type').hasClass('in')) {
        $('#login-panel .collapse').collapse('hide');
      }
      if ($('#header-navbar').hasClass('in')) {
        $('button.navbar-toggle').click();
      }
    });

  }

  init();
});

//
// NOTE:
//
// - This file contains all the new UI methods specific to 2018 revamp.
// - The legacy code has contained lots of jQuery methods. Because replacing everything is a huge task, I kept most of them as-is.
// - In each template region files, where a template-specific methods are required, invocation of relevant methods are included in the bottom area.

//
// 1. TOP NAV: CURRENT ITEM

function currentNav(nav) {
  var navItems = document.querySelectorAll(
    '.b2b-2020-nav--desktop .nav-items > li'
  );
  navItems.forEach(function(li) {
    if (
      li.innerText
        .toLowerCase()
        .trim()
        .startsWith(nav.toLowerCase())
    ) {
      li.classList.add('active');
    }
  });
}

//
// 2. MOBILE MENU

function initNav() {
  /* eslint-disable */
  (function(l){var i,s={touchend:function(){}};for(i in s)l.addEventListener(i,s)})(document); // sticky hover fix in iOS

  var resizeTimeout;
  var toggleMenu = document.getElementById('mobile-toggle');
  var loginBtn = document.querySelector('.top-ctas--login');
  var mobileSubItemTrigger = document.querySelectorAll('.b2b-mobile-nested-btn');
  var mobileTurnOff = function() {
    toggleMenu.classList.remove('is-on');
    document.body.style.overflow = '';
  };
  var mobileTurnOn = function() {
    toggleMenu.classList.add('is-on');
    document.body.style.overflow = 'hidden!important';
  };
  var mobileMenu = function() {
    toggleMenu.classList.contains('is-on') ? mobileTurnOff() : mobileTurnOn();
  };
  var collapseAllMobileToggle = function() {
    document.querySelectorAll('.b2b-mobile-nested').forEach(function(item) {
      item.style.flex ='3';
      item.style.background='white';
      var child = item.querySelector('.b2b-mobile-nested-options');
      child.classList.remove('is-on');
    });
  }
  var mobileSubItemToggle = function(e) {
    var clickedItem = e.target;
    var mobileNestedOptions = clickedItem.parentElement.querySelector(
      '.b2b-mobile-nested-options'
      );
    var mobileNestedMenu = clickedItem.parentElement;
    function expandOptions() {
      collapseAllMobileToggle();
      mobileNestedOptions.classList.add('is-on');
      mobileNestedMenu.style.flex = '5';
      mobileNestedMenu.style.background='#e1f4f7';
    }
    function collapseOptions() {
      mobileNestedOptions.classList.remove('is-on');
      mobileNestedMenu.style.flex = '3';
      mobileNestedMenu.style.background='white';
    }
    return mobileNestedOptions.classList.contains('is-on')
      ? collapseOptions()
      : expandOptions();
  };
  function loginOptionsOff() {
    loginBtn.classList.remove('is-on');
    document.querySelector('.top-ctas--login-sub').classList.remove('is-on');
  }
  function loginOptionsToggle() {
    loginBtn.classList.toggle('is-on');
    document.querySelector('.top-ctas--login-sub').classList.toggle('is-on');
  }
  function actualResizeHandler() {
    if (window.innerWidth < 900) {
      // Mobile menu break-point
      return;
    }
    loginOptionsOff();
    if (toggleMenu.classList.contains('is-on')) {
      mobileTurnOff();
    }
  }
  function resizeThrottler() {
    // ignore resize events as long as an actualResizeHandler execution is in the queue
    if (!resizeTimeout) {
      resizeTimeout = setTimeout(function() {
        resizeTimeout = null;
        actualResizeHandler();
        // The actualResizeHandler will execute at a rate of 15fps
      }, 66);
    }
  }

  document.getElementById('mobile-menu').addEventListener('click', mobileMenu);
  loginBtn.addEventListener('click', loginOptionsToggle);

  mobileSubItemTrigger.forEach(function(item){item.addEventListener('click', mobileSubItemToggle)});
  window.addEventListener('resize', resizeThrottler, false);
}

//
// 3. BLOG LIST
function blogListHandler() {
  var notFirstPage = window.location.href.indexOf('offset') !== -1;
  var filteredByAuthor = window.location.href.indexOf('author') !== -1;
  var filteredByTag = window.location.href.indexOf('tag') !== -1;
  var movingTipsBlog = window.location.href.indexOf('moving-tips') !== -1;
  var filterTitle;
  var blogListItems;
  var firstTwo;
  var firstOne;
  var mostRecentPosts;
  // Hide big two latest items contextually
  if (notFirstPage || filteredByAuthor || filteredByTag) {
    mostRecentPosts = document.querySelector('.blog-list--most-recent');
    mostRecentPosts.style.display = 'none';
    mostRecentPosts.remove();
    if (notFirstPage && !filteredByAuthor && !filteredByTag) {
      filterTitle = document.querySelector('.blog-list--filtered');
      filterTitle.style.display = 'none';
    }
  } else {
    blogListItems = document.querySelectorAll('.blog-list--item');
    firstTwo = [blogListItems[0], blogListItems[1]];
    // eslint-disable-next-line prefer-destructuring
    firstOne = blogListItems[0];
      firstTwo.forEach(function(item) {
        item.style.display = 'none';
        item.remove();
      });
    filterTitle = document.querySelector('.blog-list--filtered');
    filterTitle.style.display = 'none';
  }
}

// Utility Function for CSS property checking
function cssPropertyValueSupported(prop, value) {
  // NOTE: check if the current browser supports a CSS property
  // e.g. cssPropertyValueSupportyed('position', 'sticky') will return a Boolean
  var d = document.createElement('div');
  d.style[prop] = value;
  return d.style[prop] === value;
}

//
// 4. BLOG STICKY SIDEBAR

//
// 5. Sticky Subnav: Relo products and Resources
function jumpToStick(e) {
  // NOTE: Add native CSS scroll behavior

  e.preventDefault();
  var targetElem = document.querySelector(e.target.hash);
  window.scrollTo({
    top: targetElem.offsetTop - 120,
    behavior: 'smooth'
  });
}

function stickySubnav() {
  var stickyWrapper = document.querySelector('.revamp-2018--sticky-wrapper');
  var stickyNavTop = stickyWrapper.offsetTop;
  function stickyPos() {
    var isInvestorPage = window.location.href.indexOf('investor') !== -1;
    var isWiderThan900 = window.innerWidth > 900;
    var navbarAdjustment = 77;
    var highLightedNavItem = document.querySelector('.nav-items > li.active');
    // var stickyAdjustment = isWiderThan900 ? '70px' : '39px';
    var scrollTop = window.scrollY+navbarAdjustment;
    if (scrollTop > stickyNavTop) {
      stickyWrapper.style.position = 'fixed';
      stickyWrapper.style.top = (navbarAdjustment - 39) + 'px';
      stickyWrapper.style.width = '100%';
      stickyWrapper.style.zIndex =11;

      document.querySelector('.first-subsection').style.marginTop = '90px';
      if (!isInvestorPage && highLightedNavItem) {
        highLightedNavItem.style.borderBottom = '5px solid #334D5C';
      }
    } else {
      stickyWrapper.removeAttribute('style');
      document.querySelector('.first-subsection').style.marginTop = 0;
      if (!isInvestorPage && highLightedNavItem) {
        highLightedNavItem.style.borderBottom = '5px solid #F5333F';
      }
    }
  }

  document.querySelectorAll('.revamp-2018--sticky li').forEach(function(li) {
    li.addEventListener('click', jumpToStick);
  });
  window.addEventListener('scroll', stickyPos);
}

//
// 6. Toggle Features: Relo Solutions

function toggleReFeatures() {
  var toggleItems = document.querySelectorAll(
    '.business-re--features-list li, .mover-app--features-list li'
  );
  var toggleContents = document.querySelectorAll(
    '.business-re--features-content, .mover-app--features-content'
  );
  function toggleSections(e) {
    // console.log(e.target.id, e.target);
    if (e.target.classList.contains('active')) {
      return;
    }
    toggleItems.forEach(function(item) {
      item.classList.remove('active');
      if (item.classList.contains(e.target.id)) {
        // console.log(item);
        item.classList.add('active');
      }
    });
    document.getElementById(e.target.id).classList.add('active');
    toggleContents.forEach(function(item) {
      item.classList.remove('active');

      if (item.classList.contains(e.target.id)) {
        item.classList.add('active');
      }
    });
  }
  toggleItems.forEach(function(item) {
    item.addEventListener('click', toggleSections);
  });
}

//
// 7. Business Products circle elements
// NOTE: 
// - Unused bits from older iteration, thus removed.
// - Refer to upd-b-2019-xxx files to retrieve


//
// 9. Flip card on home page
// NOTE: 
// - Unused bits from older iteration, thus removed.
// - Refer to upd-b-2019-xxx files to retrieve

//
// 10. Kandela forms
// NOTE: 
// - Unused bits from older iteration, thus removed.
// - Refer to upd-b-2019-xxx files to retrieve

//
// 11. Kandela in your area
// NOTE: 
// - Unused bits from older iteration, thus removed.
// - Refer to upd-b-2019-xxx files to retrieve

// Sticky CTAs
// TODO: Clean up per D2C updates

function cta_top_and_bottom_nav(cta_type, option) {
  var navbar = document.querySelector('.b2b-2020-header');
  var firstSection = document.querySelector('section:first-of-type');
  var navWrapper = document.querySelector('.b2b-2020-nav--wrapper');

  var originalNavbarHeight = navbar.offsetHeight;
  var ctaHeight = 50;
  var cta_on_top = document.querySelector('div.cta_above_top_nav');
  var cta_on_bottom = document.querySelector('div.cta_on_page_bottom');
  var mobileMenuOverlay = document.querySelector('.b2b-2020--mobile-menu');
  function hideCallToOrder() {
    document
      .querySelectorAll('.cta_on_page_bottom > .cta-call-to-order')
      .forEach(function(item) {
        item.style.display = 'none';
      });
  }
  function hideMovingSupport() {
    document
      .querySelectorAll('.cta_on_page_bottom > .cta-moving-support')
      .forEach(function(item) {
        item.style.display = 'none';
      });
  }

  cta_type === 'moving-support' ? hideCallToOrder() : hideMovingSupport();

  if (option !== 'no-top') {
    cta_on_top.style.display = 'block';
  }
  cta_on_bottom.style.display = 'block';

  navbar.insertBefore(cta_on_top, navWrapper);

  function hideCTAonBottom() {
    cta_on_bottom.style.transform = 'translateY(50px)';
  }
  function showCTAonBottom() {
    var heightAdjustment = 0;
    cta_on_bottom.style.transform = "translateY(-"+ heightAdjustment + "px)";
  }
  function showCTAonTop() {
    cta_on_top.style.transform = "translateY(0)"
    navbar.style.height = originalNavbarHeight + ctaHeight + "px";
    navbar.style.paddingTop = ctaHeight + "px";
    firstSection.style.transform = "translateY("+ ctaHeight  +"px)";
    mobileMenuOverlay.style.top = originalNavbarHeight + ctaHeight + "px";
    hideCTAonBottom();
  }
  function hideCTAonTop() {
    cta_on_top.style.transform = "translateY(-" + ctaHeight+ "px)";
    navbar.style.height = originalNavbarHeight + "px";
    navbar.style.paddingTop = "0";
    firstSection.style.transform = "translateY(0)";
    mobileMenuOverlay.style.top = originalNavbarHeight + "px";
    showCTAonBottom();
  }
  function checkScroll(e) {
    if (window.scrollY >= 500) {
      hideCTAonTop();
    } else {
      option !== 'no-top' ? showCTAonTop() : hideCTAonTop();
    }
  }
  function initCTAonTop() {
    navbar.style.transition = 'all .2s';
    firstSection.style.transition = 'all .2s';
    cta_on_top.style.height = ctaHeight + "px";
    cta_on_top.style.lineHeight = ctaHeight + "px";
    option !== 'no-top' ? showCTAonTop() : hideCTAonTop();
  }
  initCTAonTop();

  window.addEventListener('scroll', checkScroll);
}


// LAZYLOAD IMAGES

function lazyInit() {
  var imageArray;

  function isInViewport(el) {
    var rect = el.getBoundingClientRect();
    return (
      rect.bottom >= 0 &&
      rect.right >= 0 &&

      rect.top <= (
        window.innerHeight ||
        document.documentElement.clientHeight) &&

      rect.left <= (
        window.innerWidth ||
        document.documentElement.clientWidth)
    );
  }

  function loadNow() {
    imageArray.forEach(function (image) {
      if (isInViewport(image)) {
        if (image.dataset.lazysrc) {
          image.src = image.dataset.lazysrc;
          image.removeAttribute('data-lazysrc');
        }
      }
    });
    imageArray = imageArray.filter(function (image) {
      return image.dataset.lazysrc;
    })
  }
  var imgNodelist = document.querySelectorAll('#content img');
  imageArray = Array.prototype.slice.call(imgNodelist);
  imageArray.forEach(function (image) {
    if (isInViewport(image)) {
      return
    }
    image.dataset.lazysrc = image.src || image.dataset.src;
    image.setAttribute('loading', 'lazy');
    image.removeAttribute('src');
    // image.src = '';
  })
  window.addEventListener('scroll', loadNow);

}

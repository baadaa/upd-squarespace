<section class="blog-list">
  <div class="breadcrumb-2018">
    <nav class="container-2018">
      <ul>
        <li><a href="/resources/">Resources</a></li>
        <li><a href="/moving-tips/">Moving Tips</a></li>
      </ul>
    </nav>
  </div>
  <h1>Moving Tips</h1>
  <h4>Realistic moving advice, tips, and tricks you can use</h4>
  <section class="blog-list--most-recent container-2018">
    <article class="blog-list--recent-item blog-list--find-a-mover">
      <h3>Find a Certified Moving Company</h3>
      <h6>Book a move, and get an invite to Updater app</h6>
      <form method="get" action="/find-a-moving-company" class="blog-list--find-a-mover-form">
        <input id="searchMoverField" class="blog-list-mover-search" type="text" name="locate"
          placeholder="Moving from... (zip or city)">
        <input type="submit" value="Find" class="blog-list-mover-search-button">
      </form>
    </article>
    <squarespace:query collection="moving-tips" limit="1">
      {.repeated section items}
      <article class="blog-list--recent-item">
        <a href="{fullUrl}" class="blog-list--recent-thumb"
          style="background-image: url('https://res.cloudinary.com/updater-marketing/image/fetch/h_250,c_fill/{assetUrl}');">&nbsp;
        </a>
        <h3 class="tips">
          <a href="{fullUrl}">{title}</a>
        </h3>
        <span class="blog-list--read-more">
          <a href="{fullUrl}">Read more ></a>
        </span>
      </article>
      {.end}
    </squarespace:query>
  </section>
  <section class="blog-list--secondary-action-wrapper">
    <div class="container-2018 blog-list--secondary-actions">
      <div class="blog-list--social">
        <span>Connect with us</span>
        <a class="blog-list--social-link" href="https://linkedin.com/company/updater" target="_blank"
          rel="noreferrer noopener">
          <svg class="blog-list--social-icon">
            <use xlink:href="/assets/social-svg-icon-defs.svg#svg-linkedin-icon"></use>
          </svg>
        </a>
        <a class="blog-list--social-link" href="https://facebook.com/updater" target="_blank" rel="noreferrer noopener">
          <svg class="blog-list--social-icon">
            <use xlink:href="/assets/social-svg-icon-defs.svg#svg-facebook-icon"></use>
          </svg>
        </a>
        <a class="blog-list--social-link" href="https://twitter.com/updater" target="_blank" rel="noreferrer noopener">
          <svg class="blog-list--social-icon">
            <use xlink:href="/assets/social-svg-icon-defs.svg#svg-twitter-icon"></use>
          </svg>
        </a>
        <a class="blog-list--social-link" href="https://www.instagram.com/updaterhq/" target="_blank"
          rel="noreferrer noopener">
          <svg class="blog-list--social-icon">
            <use xlink:href="/assets/social-svg-icon-defs.svg#svg-instagram-icon"></use>
          </svg>
        </a>
      </div>
      <div class="blog-list--search">
        <squarespace:block-field id="blogsearch" />
      </div>
    </div>
  </section>

  <section class="blog-list--filtered">
    <!-- TODO: Stylize this section -->
    {.if authorFilter}
    Articles by:{.if author.avatarId}
    <img src="https://static1.squarespace.com/static/images/{author.avatarId}/100w" /> {.or}
    <img src="https://updater.com/universal/images-v6/default-avatar.png" /> {.end}
    <span class="blog-list--filter-option">{author.displayName}</span>
    <a href="/moving-tips/">(Show all articles)</a>
    {.end}

    {.section tagFilter}
    Articles with the tag:
    <span class="blog-list--filter-option">{@|safe}</span><a href="/moving-tips/">(Show all articles)</a>
    {.end}

  </section>

  <section class="blog-list--article-list container-2018">
    {.repeated section items}

    <article id="post-{id}" class="{@|item-classes} blog-list--item">
      {.main-image?}
      <a href="{fullUrl}"">
        <img src=" https://res.cloudinary.com/updater-marketing/image/fetch/w_355,h_220,c_fill,f_auto/{assetUrl}" />
      </a>
      {.end}
      <h3 class="blog-list-title tips">
        {.passthrough?}
        <a href="{sourceUrl}"">{title}</a>
        {.or}
        <a href=" {fullUrl}"">{title}</a>
        {.end}
      </h3>
      <span class="blog-list--read-more">
        <a href="{fullUrl}">Read more ></a>
      </span>
      {postItemInjectCode}
    </article>
    {.or} No blog posts yet. {.end}
  </section>
  <nav class="blog-list--pagination container-2018">
    <ul>
      {.if pagination.prevPage}
      <li class="newer-post"><a href="{pagination.prevPageUrl}">
          < Newer Posts</a> </li> {.or} {.end} {.if pagination.nextPage} <li><a href="{pagination.nextPageUrl}">Older
              Posts ></a></li>
      {.or} {.end}
    </ul>
  </nav>
</section>

<script src="/scripts/upd-b-2018.js"></script>
<script>
  function init() {
    currentNav('Resources'); // Set current item in top nav
    initNav(); // Initialize mobile menu on smaller viewport
    blogListHandler(); // Blog list-specific DOM adjustments
    document.querySelector('input.search-input').placeholder = "Search Moving Tips...";
    cta_top_and_bottom_nav('moving-support', 'no-top');

  }
  window.onload = function () {
    init();
  }
</script>
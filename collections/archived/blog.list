<section class="blog-list">
  <div class="breadcrumb-2018">
    <nav class="container-2018">
      <ul>
        <li><a href="/resources/">Resources</a></li>
        <li><a href="/blog/">Real Estate Insights</a></li>
      </ul>
    </nav>
  </div>
  <h1>Real Estate Insights</h1>
  <h4>The ideas that shape the future of our industry</h4>
  <section class="blog-list--most-recent container-2018">
    <squarespace:query collection="blog" limit="2">
      {.repeated section items}
      <article class="blog-list--recent-item">
        <a href="{fullUrl}" class="blog-list--recent-thumb"
          style="background-image: url('https://res.cloudinary.com/updater-marketing/image/fetch/h_250,c_fill/{assetUrl}');">&nbsp;
        </a>
        <span class="blog-list--recent-item-date">
          <time datetime="{addedOn|date %F}">{addedOn|date %B %d, %Y}</time>
        </span>
        <h3>
          <a href="{fullUrl}">{title}</a>
        </h3>
        <span class="blog-list--read-more">
          <a href="{fullUrl}">Read more ></a>
        </span>
      </article>
      {.end}
    </squarespace:query>
  </section>
  <section class="blog-list--secondary-action-wrapper">
    <div class="container-2018 blog-list--secondary-actions">
      <div class="blog-list--social">
        <span>Connect with us</span>
        <a class="blog-list--social-link" href="https://linkedin.com/company/updater" target="_blank"
          rel="noreferrer noopener">
          <svg class="blog-list--social-icon">
            <use xlink:href="/assets/social-svg-icon-defs.svg#svg-linkedin-icon"></use>
          </svg>
        </a>
        <a class="blog-list--social-link" href="https://facebook.com/updater" target="_blank" rel="noreferrer noopener">
          <svg class="blog-list--social-icon">
            <use xlink:href="/assets/social-svg-icon-defs.svg#svg-facebook-icon"></use>
          </svg>
        </a>
        <a class="blog-list--social-link" href="https://twitter.com/updater" target="_blank" rel="noreferrer noopener">
          <svg class="blog-list--social-icon">
            <use xlink:href="/assets/social-svg-icon-defs.svg#svg-twitter-icon"></use>
          </svg>
        </a>
        <a class="blog-list--social-link" href="https://www.instagram.com/updaterhq/" target="_blank"
          rel="noreferrer noopener">
          <svg class="blog-list--social-icon">
            <use xlink:href="/assets/social-svg-icon-defs.svg#svg-instagram-icon"></use>
          </svg>
        </a>
        <a class="blog-list--cta" href="/moving-tips/">Read Moving Tips & Tricks Blog</a>
      </div>
      <div class="blog-list--search">
        <squarespace:block-field id="blogsearch" />
      </div>
    </div>
  </section>

  <section class="blog-list--filtered">
    <!-- TODO: Stylize this section -->
    {.if authorFilter}
    Articles by:{.if author.avatarId}
    <img src="https://static1.squarespace.com/static/images/{author.avatarId}/100w" /> {.or}
    <img src="https://updater.com/universal/images-v6/default-avatar.png" /> {.end}
    <span class="blog-list--filter-option">{author.displayName}</span>
    <a href="/blog/">(Show all articles)</a>
    {.end}

    {.section tagFilter}
    Articles with the tag:
    <span class="blog-list--filter-option">{@|safe}</span><a href="/blog/">(Show all articles)</a>
    {.end}

  </section>

  <section class="blog-list--article-list container-2018">
    {.repeated section items}

    <article id="post-{id}" class="{@|item-classes} blog-list--item">
      {.main-image?}
      <a href="{fullUrl}"">
        <img src=" https://res.cloudinary.com/updater-marketing/image/fetch/w_355,h_220,c_fill,f_auto/{assetUrl}" />
      </a>
      {.end}
      <span class="blog-list--item-date">
        <time datetime="{addedOn|date %F}">{addedOn|date %B %d, %Y}</time>
      </span>
      <h3 class="blog-list-title">
        {.passthrough?}
        <a href="{sourceUrl}"">{title}</a>
        {.or}
        <a href=" {fullUrl}"">{title}</a>
        {.end}
      </h3>
      <span class="blog-list--read-more">
        <a href="{fullUrl}">Read more ></a>
      </span>
      {postItemInjectCode}
    </article>
    {.or} No blog posts yet. {.end}
  </section>
  <nav class="blog-list--pagination container-2018">
    <ul>
      {.if pagination.prevPage}
      <li class="newer-post"><a href="{pagination.prevPageUrl}">
          < Newer Posts</a> </li> {.or} {.end} {.if pagination.nextPage} <li><a href="{pagination.nextPageUrl}">Older
              Posts ></a></li>
      {.or} {.end}
    </ul>
  </nav>
</section>

<script src="/scripts/upd-b-2020-01.min.js"></script>
<script>
  function init() {
    // currentNav('Resources'); // Set current item in top nav
    initNav('b2b'); // Initialize mobile menu on smaller viewport
    blogListHandler(); // Blog list-specific DOM adjustments
    document.querySelector('input.search-input').placeholder = "Search Industry Insights...";
  }
  window.onload = function () {
    init();
  }
</script>
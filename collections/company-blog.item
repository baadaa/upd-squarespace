<article class="blog-article">
  <div class="breadcrumb-2018">
    <nav class="container-2018">
      <ul>
        <li><a href="/about-us/">About</a></li>
        <li><a href="/updater-life/">Updater Life</a></li>
      </ul>
    </nav>
  </div>
  {.section item}
  <div class="blog-article--content-area container-2018">
    <section id="post-{id}" class="{@|item-classes} blog-article--main-content">
      <h1 class="blog-article--post-title">
        {.passthrough?} {title} {.or} {title} {.end}
      </h1>
      <div class="blog-article--meta-info">
        <span class="blog-article--post-meta">
          Published <br />
          <time title="Originally published {addedOn|date %B %d, %Y}" datetime="{addedOn|date %F}">{addedOn|date %B
            %d, %Y}
        </span>
        <div class="blog-article--reading-time">
          <span class="reading-time">0</span>&nbsp;min&nbsp;read
        </div>
      </div>
      <div class="wo_cloudinary__blog-hero-wrapper">
        <img src="{assetUrl}?format=1000w" class="blog-article--main-image wo_cloudinary__blog-hero" alt="{title}">
      </div>
      <section class="blog-article--author-byline">
        {.if author.avatarId}
        <img src="https://static1.squarespace.com/static/images/{author.avatarId}/300w" /> {.or}
        <img src="https://updater.com/universal/images-v6/default-avatar.png" /> {.end} by
        <a href="{collection.fullUrl}?author={author.id}"> {author.displayName}</a>
      </section>
      {body}
      <section class="blog-article--tags">
        <ul>
          {.repeated section tags}
          <li>
            <a href="{collection.fullUrl}?tag={@|url-encode}">{@}</a>
          </li>
          {.alternates with} {.end}
        </ul>
      </section>
      <hr>
      {@|like-button}
      <section class="blog-article--author-bio stick-stop">
        {.if author.avatarId}
        <img src="https://static1.squarespace.com/static/images/{author.avatarId}/300w" /> {.or}
        <img src="https://updater.com/universal/images-v6/default-avatar.png" /> {.end}
        <section class="blog-article--author-bio-text">

          <a href="{collection.fullUrl}?author={author.id}">{author.displayName}</a>
          {author.bio}
        </section>
      </section>
    </section>
    <aside class="blog-article--sidebar">
      <div class="stick">
        <!-- TODO: FIX STICKY SIDEBAR -->
        <squarespace:block-field id="updater-life-banner" columns="1" />
        <div class="sidebar-boxes sb-social" style="border: 1px solid #EEE; margin-top: 10px;">
          <h6 style="text-align: center; margin-bottom: 0;">Connect with us</h6>
          <div class="updater-life--social">
            <a href="https://facebook.com/updater" target="_blank" rel="noreferrer noopener">
              <svg class="foot-social">
                <use xlink:href="/assets/social-svg-icon-defs.svg#svg-facebook-icon"></use>
              </svg>
            </a>
            <a href="https://twitter.com/updater" target="_blank" rel="noreferrer noopener"> <svg class="foot-social">
                <use xlink:href="/assets/social-svg-icon-defs.svg#svg-twitter-icon"></use>
              </svg></a>
            <a href="https://www.instagram.com/updaterhq/" target="_blank" rel="noreferrer noopener"> <svg
                class="foot-social">
                <use xlink:href="/assets/social-svg-icon-defs.svg#svg-instagram-icon"></use>
              </svg></a>
            <a href="https://linkedin.com/company/updater" target="_blank" rel="noreferrer noopener"> <svg
                class="foot-social">
                <use xlink:href="/assets/social-svg-icon-defs.svg#svg-linkedin-icon"></use>
              </svg></a>
          </div>
        </div>
        <section class="blog-article--sidebar-related">
          <h5>You may also like:</h5>
          <squarespace:query collection="updater-life" tag="{.section tags}{@}{.end}" limit="3" skip="5">
            {.repeated section items}
            <article class="blog-article--sidebar-related-item">
              <a href="{fullUrl}">
                <img class="wo_cloudinary__blog-sidebar" src="{assetUrl}?format=300w" alt="{title}">
              </a>
              <a href="{fullUrl}">
                <h6>{title}</h6>
              </a>
            </article>
            {.end}

          </squarespace:query>
        </section>
      </div>
      <!-- end sticky -->
      <div class="stick-catch"></div>

    </aside>
    <section class="blog-article--footer-related container-2018">
      <h3>More from Updater Life:</h3>
      <section class="blog-list--article-list">
        <squarespace:query collection="updater-life" tag="{.section tags}{@}{.end}" limit="3" skip="2">
          {.repeated section items}

          <article id="post-{id}" class="{@|item-classes} blog-list--item">
            {.main-image?}
            <a href="{fullUrl}">
              <div class="wo_cloudinary__blog-footer-wrapper">
                <img src=" {assetUrl}?format=500w" class="wo_cloudinary__blog-footer" alt="{title}" />
              </div>
            </a>
            {.end}
            <span class="blog-list--item-date">
              <time datetime="{addedOn|date %F}">{addedOn|date %B %d, %Y}</time>
            </span>
            <h3 class="blog-list-title">
              {.passthrough?}
              <a href="{sourceUrl}"">{title}</a>
            {.or}
            <a href=" {fullUrl}"">{title}</a>
              {.end}
            </h3>
            <span class="blog-list--read-more">
              <a href="{fullUrl}">Read more ></a>
            </span>
            {postItemInjectCode}
          </article>
          {.or} No blog posts yet. {.end}
        </squarespace:query>
      </section>
      <a class="blog-list--pagination-browse-more" href="/updater-life">More Updater Life</a>

    </section>
    <section class="blog-article--comments container-2018">
      {@|comments}
    </section>
  </div>
  {.end}
</article>

<nav class="blog-list--pagination container-2018">
  {.section pagination}
  <ul>
    {.section prevItem}
    <li class="newer-post"><a href="{fullUrl}">
        < Newer Post</a> </li> {.or} {.end} {.section nextItem} <li><a href="{fullUrl}">Older Post ></a></li>
    {.or} {.end}
  </ul>
  {.end}
</nav>

<squarespace:script src="stickybits.min.js" combo="false" />
<squarespace:script src="upd-ui-bits_20_07.min.js" combo="true" />
<squarespace:script src="upd-blog-bits.min.js" combo="true" />
<script type="text/javascript">
  function init() {
    initNav();
    setReadingTime();
    lazyInit();
    initScrollIndicator();
    stickybits('.stick', {
      stickyBitStickyOffset: 90
    });

  }
  window.addEventListener('DOMContentLoaded', init);
</script>
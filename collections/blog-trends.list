<section class="blog-list">
  <div class="breadcrumb-2018">
    <nav class="container-2018">
      <ul>
        <li><a href="/resources/">Resources</a></li>
        <li><a href="/trends/">National Trends</a></li>
      </ul>
    </nav>
  </div>
  <h1>National Migration Trends</h1>
  <h4>Unique and exclusive relocation new trends and data</h4>
  <section class="blog-list--most-recent container-2018">
    <squarespace:query collection="trends" limit="2">
      {.repeated section items}
      <article class="blog-list--recent-item">
        <a href="{fullUrl}">
          <div class="wo_cloudinary__blog-list-wrapper">
            <img class="wo_cloudinary__blog-list" src="{assetUrl}?format=1000w" alt="{title}" />
          </div>
        </a>
        <h3 class="tips">
          <a href="{fullUrl}">{title}</a>
        </h3>
        <span class="blog-list--read-more">
          <a href="{fullUrl}">Read more ></a>
        </span>
      </article>
      {.end}
    </squarespace:query>
  </section>
  <section class="blog-list--secondary-action-wrapper">
    <div class="container-2018 blog-list--secondary-actions">
      <div class="blog-list--social">
        <span>Connect with us</span>
        <a class="blog-list--social-link" href="https://linkedin.com/company/updater" target="_blank"
          rel="noreferrer noopener">
          <svg class="blog-list--social-icon">
            <use xlink:href="/assets/social-svg-icon-defs.svg#svg-linkedin-icon"></use>
          </svg>
        </a>
        <a class="blog-list--social-link" href="https://facebook.com/updater" target="_blank" rel="noreferrer noopener">
          <svg class="blog-list--social-icon">
            <use xlink:href="/assets/social-svg-icon-defs.svg#svg-facebook-icon"></use>
          </svg>
        </a>
        <a class="blog-list--social-link" href="https://twitter.com/updater" target="_blank" rel="noreferrer noopener">
          <svg class="blog-list--social-icon">
            <use xlink:href="/assets/social-svg-icon-defs.svg#svg-twitter-icon"></use>
          </svg>
        </a>
        <a class="blog-list--social-link" href="https://www.instagram.com/updaterhq/" target="_blank"
          rel="noreferrer noopener">
          <svg class="blog-list--social-icon">
            <use xlink:href="/assets/social-svg-icon-defs.svg#svg-instagram-icon"></use>
          </svg>
        </a>
      </div>
      <div class="blog-list--search">
        <squarespace:block-field id="trendsearch" />
      </div>
    </div>
  </section>

  <section class="blog-list--filtered">
    <!-- TODO: Stylize this section -->
    {.if authorFilter}
    Articles by:{.if author.avatarId}
    <img src="https://static1.squarespace.com/static/images/{author.avatarId}/100w" /> {.or}
    <img src="https://updater.com/universal/images-v6/default-avatar.png" /> {.end}
    <span class="blog-list--filter-option">{author.displayName}</span>
    <a href="/trends/">(Show all articles)</a>
    {.end}

    {.section tagFilter}
    Articles with the tag:
    <span class="blog-list--filter-option">{@|safe}</span><a href="/trends/">(Show all articles)</a>
    {.end}

  </section>

  <section class="blog-list--article-list container-2018">
    {.repeated section items}

    <article id="post-{id}" class="{@|item-classes} blog-list--item">
      {.main-image?}
      <a href="{fullUrl}">
        <div class="wo_cloudinary__blog-list-wrapper">
          <img class="wo_cloudinary__blog-list" src="{assetUrl}?format=500w" />
        </div>
      </a>
      {.end}
      <h3 class="blog-list-title tips">
        {.passthrough?}
        <a href="{sourceUrl}">{title}</a>
        {.or}
        <a href="{fullUrl}">{title}</a>
        {.end}
      </h3>
      <span class="blog-list--read-more">
        <a href="{fullUrl}">Read more ></a>
      </span>
      {postItemInjectCode}
    </article>
    {.or} No blog posts yet. {.end}
  </section>
  <nav class="blog-list--pagination container-2018">
    <ul>
      {.if pagination.prevPage}
      <li class="newer-post"><a href="{pagination.prevPageUrl}">
          < Newer Posts</a> </li> {.or} {.end} {.if pagination.nextPage} <li><a href="{pagination.nextPageUrl}">Older
              Posts ></a></li>
      {.or} {.end}
    </ul>
  </nav>
</section>

<squarespace:script src="upd-ui-bits_20_07.min.js" combo="true" />
<script>
  function init() {
    initNav(); // Initialize mobile menu on smaller viewport
    blogListHandler(); // Blog list-specific DOM adjustments
    document.querySelector('input.search-input').placeholder = "Search National Trends...";
  }
  window.addEventListener('DOMContentLoaded', init);
</script>
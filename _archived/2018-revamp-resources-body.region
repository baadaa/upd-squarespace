<section class="resources-2018">
  <div class="breadcrumb-2018">
    <nav class="container-2018">
      <ul>
        <li><a href="/resources/">Resources</a></li>
      </ul>
    </nav>
  </div>
  <section class="resources-2018--hero">
    <section class="resources-2018--hero-wrapper container-2018">

      <h1>Resources</h1>
      <h4>Research, advice, tips, tricks, and more</h4>
    </section>
  </section>
  <section class="revamp-2018--sticky-wrapper">
    <nav class="container-2018 revamp-2018--sticky">
      <ul>
        <li><a href="#ii">Industry Insights</a></li>
        <li><a href="#mt">Moving Tips</a></li>
        <li><a href="#nt">Moving Trends</a></li>
        <li><a href="#ew">eBooks & Webinars</a></li>
      </ul>
    </nav>
  </section>
  <section class="resources-2018--industry-insights first-subsection" id="ii">
    <h2>Industry Insights</h2>
    <h4>The ideas that shape the future of our industry</h4>

    <section class="blog-list--article-list container-2018">
      <squarespace:query collection="blog" limit="6">
        {.repeated section items}
        <article id="post-{id}" class="{@|item-classes} blog-list--item">
          {.main-image?}
          <a href="{fullUrl}">
            <img src="{assetUrl}?format=500w" class="wo_cloudinary__resources-thumb" alt="{title}" />
          </a>
          {.end}
          <span class="blog-list--item-date">
            <time datetime="{addedOn|date %F}">{addedOn|date %B %d, %Y}</time>
          </span>
          <h3 class="blog-list-title">
            {.passthrough?}
            <a href="{sourceUrl}">{title}</a>
            {.or}
            <a href=" {fullUrl}">{title}</a>
            {.end}
          </h3>
          <span class="blog-list--read-more">
            <a href="{fullUrl}">Read more ></a>
          </span>
          {postItemInjectCode}
        </article>

        {.end}
      </squarespace:query>
      <div class="resources-2018--cta">
        <a href="/blog/">
          <span>Browse more articles</span>
        </a>
      </div>
    </section>
  </section>
  <section class="resources-2018--moving-tips" id="mt">
    <h2>Moving Tips</h2>
    <h4>Practical moving tips, tricks, and advice</h4>

    <section class="blog-list--article-list container-2018">
      <squarespace:query collection="moving-tips" limit="6">
        {.repeated section items}
        <article id="post-{id}" class="{@|item-classes} blog-list--item">
          {.main-image?}
          <a href="{fullUrl}">
            <img src="{assetUrl}?format=500w" class="wo_cloudinary__resources-thumb" alt="{title}" />
          </a>
          {.end}
          <span class="blog-list--item-date">
            <time datetime="{addedOn|date %F}">{addedOn|date %B %d, %Y}</time>
          </span>
          <h3 class="blog-list-title">
            {.passthrough?}
            <a href="{sourceUrl}">{title}</a>
            {.or}
            <a href="{fullUrl}">{title}</a>
            {.end}
          </h3>
          <span class="blog-list--read-more">
            <a href="{fullUrl}">Read more ></a>
          </span>
          {postItemInjectCode}
        </article>

        {.end}
      </squarespace:query>
      <div class="resources-2018--cta">
        <a href="/moving-tips/">
          <span>Browse more moving tips</span>
        </a>
      </div>
    </section>
  </section>
  <section class="resources-2018--trends" id="nt">
    <h2>Moving Trends</h2>
    <h4>The latest national relocation trends</h4>

    <section class="blog-list--article-list container-2018">
      <squarespace:query collection="trends" limit="6">
        {.repeated section items}
        <article id="post-{id}" class="{@|item-classes} blog-list--item">
          {.main-image?}
          <a href="{fullUrl}">
            <img src="{assetUrl}?format=500w" class="wo_cloudinary__resources-thumb" alt="{title}" />
          </a>
          {.end}
          <span class="blog-list--item-date">
            <time datetime="{addedOn|date %F}">{addedOn|date %B %d, %Y}</time>
          </span>
          <h3 class="blog-list-title">
            {.passthrough?}
            <a href="{sourceUrl}">{title}</a>
            {.or}
            <a href=" {fullUrl}">{title}</a>
            {.end}
          </h3>
          <span class="blog-list--read-more">
            <a href="{fullUrl}">Read more ></a>
          </span>
          {postItemInjectCode}
        </article>

        {.end}
      </squarespace:query>
      <div class="resources-2018--cta">
        <a href="/trends/">
          <span>Browse more trends</span>
        </a>
      </div>
    </section>
  </section>
  <section class="resources-2018--ebooks-webinars" id="ew">
    <h2>eBooks and Webinars</h2>
    <h4>Knowledge and insights from industry experts</h4>

    <section class="blog-list--article-list container-2018">
      <article class="blog-list--item">
        <span class="resources-2018--thumbnail-label tips">Webinar</span>
        <a href="/blog/resident-events-that-boost-retention-webinar">
          <img
            src=" https://res.cloudinary.com/updater-marketing/image/upload/w_355,h_220,c_fill,f_auto/v1546537362/website/2018-revamp-assets/photos/shutterstock_1092513062.jpg" />
        </a>
        <h3 class="blog-list-title">
          <a href="/blog/resident-events-that-boost-retention-webinar">Boost Resident Retention with Engaging Events</a>
        </h3>
        <span class="blog-list--read-more">
          <a href="/blog/resident-events-that-boost-retention-webinar">View the Webinar ></a>
        </span>
      </article>
      <article class="blog-list--item">
        <span class="resources-2018--thumbnail-label">eBook</span>
        <a
          href="https://res.cloudinary.com/updater-marketing/image/upload/v1489075890/downloads/property%20management/property_management_online_reviews_ebook.pdf">
          <img
            src=" https://res.cloudinary.com/updater-marketing/image/upload/w_355,h_220,c_fill,f_auto/v1545461573/website/2018-revamp-assets/photos/shutterstock_1094965409.jpg" />
        </a>
        <h3 class="blog-list-title">
          <a
            href="https://res.cloudinary.com/updater-marketing/image/upload/v1489075890/downloads/property%20management/property_management_online_reviews_ebook.pdf">How
            to
            Effectively Manage Your Property's Online Reputation</a>
        </h3>
        <span class="blog-list--read-more">
          <a
            href="https://res.cloudinary.com/updater-marketing/image/upload/v1489075890/downloads/property%20management/property_management_online_reviews_ebook.pdf">Get
            the eBook
            ></a>
        </span>
      </article>
      <article class="blog-list--item">
        <span class="resources-2018--thumbnail-label">eBook</span>
        <a
          href="https://res.cloudinary.com/updater-marketing/image/upload/v1481826517/downloads/property%20management/updater_ebook_amazing-move-ins.pdf">
          <img
            src=" https://res.cloudinary.com/updater-marketing/image/upload/w_355,h_220,c_fill,f_auto/v1545461574/website/2018-revamp-assets/photos/shutterstock_1125902990.jpg" />
        </a>
        <h3 class="blog-list-title">
          <a
            href="https://res.cloudinary.com/updater-marketing/image/upload/v1481826517/downloads/property%20management/updater_ebook_amazing-move-ins.pdf">Step-by-Step
            Guide to
            Amazing Move-Ins</a>
        </h3>
        <span class="blog-list--read-more">
          <a
            href="https://res.cloudinary.com/updater-marketing/image/upload/v1481826517/downloads/property%20management/updater_ebook_amazing-move-ins.pdf">Get
            the eBook ></a>
        </span>
      </article>
    </section>
  </section>

</section>

<squarespace:script src="upd-ui-bits_20_07.min.js" combo="true" />
<script>
  function init() {
    initNav(); // Initialize mobile menu on smaller viewport
  }
  window.addEventListener('DOMContentLoaded', function () {
    init();
    stickySubnav();
  });
</script>
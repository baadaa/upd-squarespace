<div class="container v2"  id="blog-post">

  {.section item}

  <div class="breadcrumbs">
    <a href="/updater-life">Updater Life Home</a> &#8250; {title}
  </div>

  <!-- PAGE CONTENT -->
  <div class="pagecontent">
    <article id="post-{id}" class="{@|item-classes}">

      <!-- CONTAINER -->
      <div class="blog-post-outer">
        <div class="blog-post-inner">

          <!--POST TILE-->

          <!-- POST TITLE AND CREDITS -->
          <div class="postheader">
            <div class="blog-date">
              <time datetime="{addedOn|date %F}">{addedOn|date %B %d, %Y}</time>
            </div>
            <h1 class="title">
              {.passthrough?} {title} {.or} {title} {.end}
            </h1>
          </div>

          <!-- excerpt -->
          {.if excerpt}
          <!-- <div class="excerpt mg-top-small">{excerpt}</div> -->
          {.end}

          <!-- HERO IMAGE -->
          <div class="hero" style="background: url('https://res.cloudinary.com/updater-marketing/image/fetch/c_limit,f_auto,q_75,w_1750/{assetUrl}'); background-size: cover; background-repeat: no-repeat; background-position: center; width: 100%; height: 400px;"></div>

          <!-- CREDIT -->
          <div class="text-upper postedin">
            {.if author.avatarId}
            <img src="https://static1.squarespace.com/static/images/{author.avatarId}/300w" class="img-circle img-border biopic" style="width: 50px;"
            /> {.or}
            <img src="https://updater.com/universal/images-v6/default-avatar.png" class="img-circle img-border biopic" style="width: 50px;"
            /> {.end} by
            <a href="{collection.fullUrl}?author={author.id}">{author.displayName}</a>
          </div>

          <!--MAIN CONTENT -->

          {body}

          <hr class="short">
          <div class="small text-muted mg-top-medium author">
            <strong>
              <a href="{collection.fullUrl}?author={author.id}">{author.displayName}</a>
            </strong>: {author.bio}
          </div>

          <!--BLOG INJECTION-->

          {postItemInjectCode}

          <br>
          <!--LIKE-->

          {@|like-button}

          <!-- CTA OPTIONAL CODE HERE -->
          <squarespace:block-field id="cta-{id}" columns="12" />
        </div>
      </div>
      <!-- close container -->

      <!-- SIDEBAR -->
      <div id="blog-sidebar">
        <div id="sticky">
          <!-- AD BLOCK -->
          <div class="sidebar-banner sb-banner zero-sqs">
            <squarespace:block-field id="updater-life-banner" columns="1" />
          </div>

          <!--  SOCIAL  -->
          <div class="sidebar-boxes sb-social">
            <h6 class="text-upper pad-bottom-small text-center">Connect with us</h6>
            <div class="inline-center-block text-left">
              <p>
                <a href="//facebook.com/updater" target="_blank" class="ico-social-footer ico-facebook-footer pop">Updater on Facebook</a>
                <a href="//facebook.com/updater" target="_blank" class="pop"> Facebook</a>
              </p>
              <p>
                <a href="//twitter.com/updater" target="_blank" class="ico-social-footer ico-twitter-footer pop">Updater on Twitter</a>
                <a href="//twitter.com/updater" target="_blank" class="pop"> Twitter</a>
              </p>
              <p>
                <a href="//www.instagram.com/updaterhq/" target="_blank" class="ico-social-footer ico-instagram-footer">Updater on Instagram</a>
                <a href="//www.instagram.com/updaterhq/" target="_blank" class="pop"> Instagram</a>
              </p>
              <p>
                <a href="//linkedin.com/company/updater" target="_blank" class="ico-social-footer ico-linkedin-footer pop">Updater on LinkedIn</a>
                <a href="//linkedin.com/company/updater" target="_blank" class="pop"> LinkedIn</a>
              </p>
            </div>
          </div>
          <!-- -->
		  
		  
		   <!-- YOU MAY ALSO LIKE SIDEBAR W/ PHOTO -->
		    <div class="hidden-xs">
			<p class="text-upper">You May Also Like</p>
			<squarespace:query collection="updater-life" tag="{.section tags}{@}{.end}" limit="3" skip="5">

			   <div class="related">

				{.repeated section items}
				<div class="recommended-item">
			<a href="{fullUrl}" class="thumb" style="background: url('https://res.cloudinary.com/updater-marketing/image/fetch/h_250,c_fill,f_auto/{assetUrl}'); background-size: cover; background-repeat: no-repeat; background-position: center; width: 100px; height: 100px; display: table-cell;
				vertical-align: middle; float: left; margin-right: 15px;"></a>
				<h6 class="title">
				  {.passthrough?}
					<a href="{sourceUrl}" target="_blank">{title}</a>
				  {.or}
					<a href="{fullUrl}">{title}</a>
				  {.end}
				</h6>
				<div class="clearfix"></div>
				</div>
				{.end}

				</div>
			</squarespace:query>
			</div>
			
			
        </div>
        <!-- end sticky -->
        <div id="catcher">
          <!--catcher-->
        </div>

      </div>
      <!-- end sidebar -->

      <div class="clearfix"></div>

      <!-- cta slide in optional -->
      <p id="last"></p>

      <div id="stickystop"></div>

	  <!-- MORE POSTS -->
      <h5 class="text-upper">You May Also Like</h5>
      <squarespace:query collection="updater-life" tag="{.section tags}{@}{.end}" limit="3" skip="2">
        <div class="row related">
          {.repeated section items}
          <div class="col-sm-4 pad-bottom-large text-center-mobile">
            <a href="{fullUrl}" class="thumb" style="background: url('https://res.cloudinary.com/updater-marketing/image/fetch/h_250,c_fill,f_auto/{assetUrl}'); background-size: cover; background-repeat: no-repeat; background-position: center; width: 100%; height: 200px; display: block;">
              <div class="text-overlay">
                <div class="read-more text-upper">Read More</div>
              </div>
            </a>
            <h4 class="title">
              {.passthrough?}
              <a href="{sourceUrl}" target="_blank">{title}</a>
              {.or}
              <a href="{fullUrl}">{title}</a>
              {.end}
            </h4>
          </div>
          {.end}
        </div>
      </squarespace:query>
	  
	  

      <!--TAGS MOBILE-->
      <div class="panel visible-xs" id="blog-accordions">
        <a role="button" data-toggle="collapse" href="#blog-tags" aria-expanded="false" aria-controls="collapseExample">
          <h5 class="text-upper">Topics
            <i class="fa fa-angle-down"></i>
          </h5>
        </a>
        <div class="collapse" id="blog-tags">
          {.repeated section tags}
          <a class="tag" href="{collection.fullUrl}?tag={@|url-encode}">{@}</a>{.alternates with} {.end}
          <div class="clearfix"></div>
        </div>
      </div>

      <!--TAGS DESKTOP-->
      <div class="hidden-xs">
        <h5 class="text-upper">Topics</h5>
        {.repeated section tags}
        <a class="tag" href="{collection.fullUrl}?tag={@|url-encode}">{@}</a>{.alternates with} {.end}
        <div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>

      <!--COMMENTS-->
      <div class="visible-xs">
        <a href="#" class="toggler">
          <h5 class="text-upper">Comments
            <i class="fa fa-angle-down"></i>
          </h5>
        </a>
      </div>
      <div id="comments-block">
        {@|comments} {.end}
      </div>
    </article>

    <!--PAGINATION-->
    {.section pagination}
    <nav>
      <!--NEWER PAGE-->
      <div class="row postnav">
        <div class="col-xs-4 text-left">
          {.section prevItem}
          <a href="{fullUrl}">&#8249; Previous</a>
          {.or}
          <span class="off">&#8249; Previous</span>
          {.end}
        </div>
        <div class="col-xs-4 text-center">
          <a href="/updater-life">Home</a>
          <!--OLDER PAGE-->
        </div>
        <div class="col-xs-4 text-right">
          {.section nextItem}
          <a href="{fullUrl}">Next &#8250;</a>
          {.or}
          <span class="off">Next &#8250;</span>
          {.end}
        </div>
      </div>
    </nav>
  </div>
  {.end}
</div>

<script type="text/javascript">
  $(document).ready(function () {
    function isScrolledTo(elem) {
      var docViewTop = $(window).scrollTop(); //num of pixels hidden above current screen
      var docViewBottom = docViewTop + $(window).height();
      var elemTop = $(elem).offset().top; //num of pixels above the elem
      var elemBottom = elemTop + $(elem).height();
      console.log("Elem Bottom: " + elemBottom);
      console.log("Return: " + (elemTop <= docViewTop));
      return ((elemTop <= docViewTop || elemTop >= docViewTop));
    }
    var catcher = $('#catcher');
    var sticky = $('#sticky');
    var footer = $('#stickystop');
    var footTop = footer.offset().top;
    var lastStickyTop = sticky.offset().top;
    $(window).scroll(function () {
      if (isScrolledTo(sticky)) {
        sticky.css('position', 'fixed');
        sticky.css('top', '75px');
      }
      var stopHeight = catcher.offset().top + catcher.height();
      var stickyFoot = sticky.offset().top + sticky.height();

      if (stickyFoot > footTop - 10) {
        console.log("Top of Footer");
        sticky.css({
          position: 'absolute',
          top: (footTop - 20) - sticky.height()
        });
      } else {
        if (stopHeight > sticky.offset().top) {
          console.log("Default position");
          sticky.css('position', 'absolute');
          sticky.css('top', stopHeight);
        }
      }
    });
  });
</script>


<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59f778ab850ee9a7"></script>

<!-- 031318 -->

<section class="business-re">
  <div class="breadcrumb-2018">
    <nav class="container-2018">
      <ul>
        <li><a href="/businesses/">Solutions</a></li>
        <li><a href="/solutions/moving/">Moving Companies</a></li>
      </ul>
    </nav>
  </div>
  <section class="business-re--hero business-re--hero-moving">
    <div class="container-2018">
      <h1>Showcase your moving<br>company within Updater.</h1>
      <a class="moving-hero-demo-from-hero" href="#re-request">Request a demo</a>
    </div>
  </section>
  <section class="business-re--intro moving container-2018">
    <img src="https://res.cloudinary.com/movehq/image/upload/v1525464479/ipad-moveiq-your-result-h_3_1_rkd2po.png"
      alt="Showcase Your Moving Company">
    <div>
      <h2>Updater for Moving Companies</h2>
      <p>Say hello to your 24/7 sales tool. We provide Updater users with information about your moving
        company&mdash;like
        availability and pricing&mdash;and drive real customers to your business from a high-intent audience of
        consumers
        looking for moving support.</p>
    </div>
  </section>
  <section class="revamp-2018--sticky-wrapper">
    <nav class="container-2018 revamp-2018--sticky">
      <ul>
        <li><a href="#re-users">Meet Our Users</a></li>
        <li><a href="#re-clients">Clients</a></li>
        <li><a href="#re-request">Request a Demo</a></li>
      </ul>
    </nav>
  </section>
  <section class="business-re--users first-subsection" id="re-users">
    <h2>Meet Our Users</h2>
    <section class="business-re--users-grid container-2018 moving-2018">
      <article class="business-re--users-grid-item">
        <div class="donut-chart chart1">
          <div class="slices"></div>
          <div class="slices"></div>
          <div class="slices"></div>
          <div class="slices"></div>
          <div class="num">1 in 4</div>
        </div>
        <div class="chart-description">
          <h6>moving households</h6>
          <p>in the U.S. is invited to Updater</p>
        </div>
      </article>
      <article class="business-re--users-grid-item">
        <div class="donut-chart chart2">
          <div class="slice one"></div>
          <div class="slice two"></div>
          <div class="chart-center">
            <span></span>
          </div>
        </div>
        <div class="chart-description">
          <h6>of our users</h6>
          <p>indicate they need moving assistance</p>
        </div>
      </article>
      <article class="business-re--users-grid-item">
        <div class="donut-chart chart3">
          <div class="slice one"></div>
          <div class="slice two"></div>
          <div class="chart-center">
            <span></span>
          </div>
        </div>
        <div class="chart-description">
          <p>Our users are <span>5X more likely</span> to reserve a move through us than go elsewhere</p>
        </div>
      </article>
    </section>
  </section>
  <section class="business-re--clients" id="re-clients" style="background:#FFF;">
    <section class="home-2018--feature-highlights" style="background: none; padding-top: 0;">
      <div class="container-2018">
        <h2>Client Stories</h2>
        <div class="home-2018--feature-carousel">
          <div class="home-2018--carousel" data-flickity='{ "wrapAround": true }'>
            <div class="home-2018--carousel-inner">
              <div class="left-side">
                <img
                  src="https://res.cloudinary.com/updater-marketing/image/upload/v1562099676/website/2018-revamp-assets/logos/partner-logos/molloy-bros.png"
                  alt="Molloy Bros">
                <div class="read-client-story">
                  <a href="https://www.movehq.com/blog/molloy-bros-hq-engagement" target="_blank"
                    rel="noreferrer noopener">Read the full
                    story</a>
                </div>
              </div>
              <div class="right-side">
                <span class="testimonial-words">
                  The platform increased our business. Quality reservations come through
                  and they convert faster than leads from other channels.
                </span>
                <div class="testimonial-by">
                  <img class="testimonial-headshot"
                    src="https://res.cloudinary.com/updater-marketing/image/upload/v1562099639/website/2018-revamp-assets/photos/clients-headshot/stephen-seligson.jpg">
                  <div>
                    <span class="testimonial-name">Stephen Seligson</span>
                    <span class="testimonial-location">VP of Sales & Marketing, Molloy Bros. Moving & Storage</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="home-2018--carousel-inner">
              <div class="left-side">
                <img
                  src="https://res.cloudinary.com/updater-marketing/image/upload/v1562099995/website/2018-revamp-assets/logos/partner-logos/olympia-moving.jpg"
                  alt="Olympia">
                <div class="read-client-story">
                  <a href="https://www.movehq.com/blog/olympia-and-updater" target="_blank"
                    rel="noreferrer noopener">Read
                    the full story</a>
                </div>
              </div>
              <div class="right-side">
                <span class="testimonial-words">The partnership we have with Updater enables us to deliver an
                  innovative, white-glove service to all of our clients and a no-brainer client engagement tool to our
                  brokers and agents.</span>
                <div class="testimonial-by">
                  <img class="testimonial-headshot"
                    src="https://res.cloudinary.com/updater-marketing/image/upload/v1562099902/website/2018-revamp-assets/photos/clients-headshot/rachael-fischer-lyons.jpg">
                  <div>
                    <span class="testimonial-name">Rachael Fischer Lyons</span>
                    <span class="testimonial-location">Director of Marketing & Business Development, Olympia</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="home-2018--carousel-inner">
              <div class="left-side">
                <img
                  src="https://res.cloudinary.com/updater-marketing/image/upload/v1562100574/website/2018-revamp-assets/logos/partner-logos/bellhops_logo.png"
                  alt="Bellhops">
                <div class="read-client-story">
                  <a href="https://www.movehq.com/blog/hq-engagement-bellhops" target="_blank"
                    rel="noreferrer noopener">Read the full story</a>
                </div>
              </div>
              <div class="right-side">
                <span class="testimonial-words">
                  It delivers actual customers. All we have to do is confirm and book their move. It couldn’t be easier.
                </span>
                <div class="testimonial-by">
                  <img class="testimonial-headshot"
                    src="https://res.cloudinary.com/updater-marketing/image/upload/v1562100601/website/2018-revamp-assets/photos/clients-headshot/cam-doody.jpg">
                  <div>
                    <span class="testimonial-name">Cameron Doody</span>
                    <span class="testimonial-location">Founder, President & Chairman, Bellhops</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <a href="https://www.movehq.com/clients" target="_blank" rel="noreferrer noopener"
          class="blue-btn-outline-2018">View more client
          stories</a>
      </div>
    </section>
  </section>

  <section class="business-re--form" id="re-request" style="background: #EFF3F6;">
    <div class="container-2018">
      <style>
        .sfwl_form .sfwl_row {
          flex-direction: column;
          align-items: flex-start;
        }

        .sfwl_form .sfwl_row label,
        .sfwl_form .sfwl_row input,
        .sfwl_form .sfwl_row select,
        .sfwl_form .sfwl_row textarea {
          width: 100%;
          flex-basis: auto;
          text-align: left;
        }

        .sfwl_form .sfwl_submit {
          margin-left: auto;
          margin-right: auto;
        }
      </style>
      <h2>Request a demo</h2>
      <h6>We'll get back to you within one day.</h6>
      {@|apply sfwl-moving-form.block}
    </div>
  </section>
  <section class="business-re--partners" style="margin-top: 0;">
    <h5>Proud members of</h5>
    <div class="business-re--partners-logo container-2018">
      <a href="https://www.moving.org/" target="_blank" rel="noreferrer noopener">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1529001918/MoveHQ/footer-elem/summit-award-winner-years.png"
          style="width: 200px; opacity: .7;">
      </a>
      <a href="https://www.moving.org/" target="_blank" rel="noreferrer noopener">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1528824592/MoveHQ/footer-elem/amsa-supplier.png"
          style="width: 140px; opacity: .7;">
      </a>
      <a href="https://www.moveforhunger.org/" target="_blank" rel="noreferrer noopener"><img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1570545180/partners/logos/MoveForHungerLogo_CutOutWhite.png"
          style="opacity: .8;" width="160"></a>
    </div>
  </section>
</section>
<squarespace:script src="jquery-3.5.1.min.js" combo="true" />
<squarespace:script src="featherlight.min.js" combo="true" />
<squarespace:script src="flickity.pkgd.min.js" combo="true" />
<squarespace:script src="upd-ui-bits_20_07.min.js" combo="true" />
<script>
  function init() {
    currentNav("I'm a business"); // Set current item in top nav
    initNav(); // Initialize mobile menu on smaller viewport
    stickySubnav();
    document.querySelector('.moving-hero-demo-from-hero').addEventListener('click', jumpToStick);

  }
  window.addEventListener('DOMContentLoaded', init);
</script>
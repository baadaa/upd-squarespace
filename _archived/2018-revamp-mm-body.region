<section class="business-mm">
  <div class="breadcrumb-2018">
    <nav class="container-2018">
      <ul>
        <li>How it works</li>
        <li><a href="/businesses/">For Businesses</a></li>
        <li><a href="/business-marketing/">Business Marketing</a></li>
      </ul>
    </nav>
  </div>
  <section class="business-mm--hero">
    <div class="container-2018">
      <squarespace:block-field id="mover-marketing--header-area" />
      <h1>The right message,<br>
        to the right movers,<br>
        at the right time.</h1>
      <a class="mm-partner-with-us" href="#re-request">Partner with us</a>
      <a class="mm-video" href="https://www.youtube.com/embed/WvYFMrxZAt0?rel=0&amp;autoplay=1"
        data-featherlight="iframe" data-featherlight-iframe-frameborder="0"
        data-featherlight-iframe-allow="autoplay; encrypted-media" data-featherlight-iframe-allowfullscreen="true"
        data-featherlight-iframe-style="display:block;border:none;height:85vh;width:85vw;"><svg class="triangle"
          xmlns="http://www.w3.org/2000/svg" viewBox="0 0 103 121">
          <polygon points="0 121 0 0 103 61 0 121" /></svg><span class="text-label">Watch video</span></a>

      <a href="https://www.youtube.com/embed/WvYFMrxZAt0?rel=0&amp;autoplay=1" data-featherlight="iframe"
        data-featherlight-iframe-frameborder="0" data-featherlight-iframe-allow="autoplay; encrypted-media"
        data-featherlight-iframe-allowfullscreen="true"
        data-featherlight-iframe-style="display:block;border:none;height:85vh;width:85vw;">
        <svg class="circle-triangle" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 193 193">
          <path d="M96.5,0A96.5,96.5,0,1,0,193,96.5,96.5,96.5,0,0,0,96.5,0ZM70,137.3V55.7l69.5,41.1Z" /></svg>
      </a>
    </div>
  </section>
  <section class="business-mm--intro">
    <h3>Updater is designed to help businesses<br>intelligently communicate with<br> millions of movers, <em>weeks</em>
      before they move.</h3>
  </section>
  <section class="business-mm--ebook">
    <squarespace:block-field id="mover-marketing--body-content" />
    <section class="business-mm--ebook-wrapper container-2018">
      <div class="book-illo"></div>
      <div class="book-intro">
        <h4>Free eBook</h4>
        <h2>How to Hit a Moving Target</h2>
        <p>See how top brand marketers reach an ever-elusive audience&mdash;people that are moving. Movers have a
          massive to-do list. In the weeks leading up to their move, they're stressed and running from business to
          business with a "get it done" attitude. Yet, during those crucial weeks, businesses often struggle to
          effectively communicate their products, services and value propositions. It's time to rethink the whole
          process.</p>
        <p>In this eBook, you'll learn how to:<p>
            <ul>
              <li>Evaluate movers' unique buying behaviors</li>
              <li>Understand the mover mindset over time and throughout the move lifecycle</li>
              <li>Upgrade your current strategies to proven and powerful techniques</li>
            </ul>
            <button class="ebook-request" disabled>Coming soon</button>
      </div>
      <div class="business-mm--ebook-request">
        <!-- <script src="//app-ab03.marketo.com/js/forms2/js/forms2.min.js"></script>
        <form id="mktoForm_1908"></form>
        <script>
          MktoForms2.loadForm("//app-ab03.marketo.com", "609-SUY-969", 1908);
        </script> -->
        <span class="modal-close">Cancel</span>


      </div>
    </section>
  </section>
  <section class="business-mm--great-for">
    <h2>Who can integrate with Updater?</h2>
    <div class="business-mm--great-for-grid container-2018">
      <div class="grid-item">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1546236813/website/2018-revamp-assets/icons/business-verticals/verticals-banks-circle-outline.svg"
          alt="Banks & financial institution">
        Banks & financial<br>institutions
      </div>
      <div class="grid-item">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1545073999/website/2018-revamp-assets/icons/app-features/icon-app-feature-insurance-circle-outline.svg"
          alt="Banks & financial institution">
        Insurance<br>companies
      </div>
      <div class="grid-item">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1546236813/website/2018-revamp-assets/icons/business-verticals/verticals-entertinment-circle-outline.svg"
          alt="Entertainment">
        Entertainment<br>companies
      </div>
      <div class="grid-item">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1546236815/website/2018-revamp-assets/icons/business-verticals/verticals-grocery-circle-outline.svg"
          alt="Grocery & Restaurant">
        Grocery stores<br>& restaurants
      </div>
      <div class="grid-item">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1546236816/website/2018-revamp-assets/icons/business-verticals/verticals-health-circle-outline.svg"
          alt="Health and personal services">
        Health, child,<br>& personal services
      </div>
      <div class="grid-item">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1546236817/website/2018-revamp-assets/icons/business-verticals/verticals-home-service-circle-outline.svg"
          alt="Home-related services">
        Home-related<br>services
      </div>
      <div class="grid-item">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1546236818/website/2018-revamp-assets/icons/business-verticals/verticals-local-business-circle-outline.svg"
          alt="Local businesses">
        Local<br>businesses
      </div>
      <div class="grid-item">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1546236820/website/2018-revamp-assets/icons/business-verticals/verticals-pet-circle-outline.svg"
          alt="Pet services">
        Pet<br>services
      </div>
      <div class="grid-item">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1546236821/website/2018-revamp-assets/icons/business-verticals/verticals-pharmacy-circle-outline.svg"
          alt="Pharmacy">
        Pharmacies
      </div>
      <div class="grid-item">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1546236822/website/2018-revamp-assets/icons/business-verticals/verticals-retail-circle-outline.svg"
          alt="Retailers">
        Retailers
      </div>
      <div class="grid-item">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1546236823/website/2018-revamp-assets/icons/business-verticals/verticals-tv-internet-circle-outline.svg"
          alt="TV & Internet">
        TV & internet<br>providers
      </div>
      <div class="grid-item">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1546236824/website/2018-revamp-assets/icons/business-verticals/verticals-utility-circle-outline.svg"
          alt="Utility">
        Utility<br>companies
      </div>

    </div>
  </section>
  <section class="business-re--form container-2018" id="re-request">
    <style>
      .sfwl_form .sfwl_row {
        flex-direction: column;
        align-items: flex-start;
      }

      .sfwl_form .sfwl_row label,
      .sfwl_form .sfwl_row input,
      .sfwl_form .sfwl_row select,
      .sfwl_form .sfwl_row textarea {
        width: 100%;
        flex-basis: auto;
        text-align: left;
      }

      .sfwl_form .sfwl_submit {
        margin-left: auto;
        margin-right: auto;
      }
    </style>

    <h2>Partner with us</h2>
    {@|apply sfwl-all-business-form.block}
  </section>
</section>

<squarespace:script src="jquery-3.5.1.min.js" combo="true" />
<squarespace:script src="featherlight.min.js" combo="true" />
<squarespace:script src="flickity.pkgd.min.js" combo="true" />
<squarespace:script src="upd-ui-bits_20_07.min.js" combo="true" />
<script>
  function handleJump(e) {
    e.preventDefault();
    // console.log(e, e.target, e.target.hash);
    var targetElem = document.querySelector(e.target.hash);
    window.scrollTo({
      top: targetElem.offsetTop - 120,
      behavior: 'smooth'
    })
  }

  function init() {

    function pullContent() {
      var hero_title = document.querySelector('.business-mm--hero h1');
      var hero_cta_btn = document.querySelector('.business-mm--hero .mm-partner-with-us');
      var hero_video_btn = document.querySelector('.business-mm--hero .mm-video .text-label');

      hero_title.innerHTML = mm_heroSection.title;
      hero_cta_btn.innerHTML = mm_heroSection.cta_btn;
      hero_video_btn.innerHTML = mm_heroSection.video_btn;

      var leadInCopyBlock = document.querySelector('.business-mm--intro h3');

      leadInCopyBlock.innerHTML = mm_leadnInCopy;

      var ebookSectionHeading = document.querySelector('.business-mm--ebook .book-intro h4');
      var ebookBookTitle = document.querySelector('.business-mm--ebook .book-intro h2');
      var ebookBookIntro = document.querySelector('.business-mm--ebook .book-intro h2+p');
      var ebookBulletTitle = document.querySelector('.business-mm--ebook .book-intro p+p');
      var ebookBulletList = document.querySelector('.business-mm--ebook .book-intro ul');
      var ebookButton = document.querySelector('.business-mm--ebook .book-intro .ebook-request');

      ebookSectionHeading.innerHTML = mm_ebookSection.heading;
      ebookBookTitle.innerHTML = mm_ebookSection.bookTitle;
      ebookBookIntro.innerHTML = mm_ebookSection.introText;
      ebookBulletTitle.innerHTML = mm_ebookSection.bulletTitle;
      ebookBulletList.innerHTML = mm_ebookSection.bulletList.map(function (item) {
        return "<li>" + item + "</li>";
      }).join('');
      ebookButton.innerHTML = mm_ebookSection.cta_btn;
    }

    currentNav("I'm a business"); // Set current item in top nav
    initNav(); // Initialize mobile menu on smaller viewport

    function ebookLaunch(e) {
      document.querySelector('.business-mm--ebook-request').classList.add('active');
    }

    function ebookClose(e) {
      document.querySelector('.business-mm--ebook-request').classList.remove('active');
    }
    pullContent();
    // document.querySelector('.ebook-request').addEventListener('click', ebookLaunch);
    document.querySelector('.modal-close').addEventListener('click', ebookClose);
    document.querySelector('a.mm-partner-with-us').addEventListener('click', handleJump);
  }
  window.addEventListener('DOMContentLoaded', init);
</script>